(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab5-tab5-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab5/tab5.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab5/tab5.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <!-- <img src=\"../../assets/images/logo.png\" alt=\"logo\" class=\"header_logo\"> -->\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        ANNOUNCEMENTS\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">\r\n        <!-- <ion-icon name=\"information-circle-outline\" size=\"medium\"></ion-icon> -->\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n  <div class=\"wrapper\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\" *ngFor=\"let Event of LetestEventsDatas\">\r\n          <div class=\"blog_card\"  (click)=\"go(Event)\">\r\n            <!-- <div class=\"blog_card_img\" style=\"background: url('{{getUrl(Event.image)}}');\"></div> -->\r\n            <div class=\"blog_card_img\" [ngStyle]=\"{ 'background-image': 'url(' + Event.image + ')'}\"></div>\r\n            <div class=\"clearfix\"></div>\r\n            <div class=\"blog_card_body\">\r\n              <!-- <span class=\"category\"> New EVENT</span> -->\r\n              <h2>{{Event.title}}</h2>\r\n              <span class=\"date\">{{GetDateTime(Event.pubdate)}}</span>\r\n              <p>{{Event.description}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n        \r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/tab5/tab5-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tab5/tab5-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab5PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab5PageRoutingModule", function() { return Tab5PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tab5_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab5.page */ "./src/app/tab5/tab5.page.ts");




const routes = [
    {
        path: '',
        component: _tab5_page__WEBPACK_IMPORTED_MODULE_3__["Tab5Page"]
    }
];
let Tab5PageRoutingModule = class Tab5PageRoutingModule {
};
Tab5PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Tab5PageRoutingModule);



/***/ }),

/***/ "./src/app/tab5/tab5.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab5/tab5.module.ts ***!
  \*************************************/
/*! exports provided: Tab5PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab5PageModule", function() { return Tab5PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _tab5_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab5-routing.module */ "./src/app/tab5/tab5-routing.module.ts");
/* harmony import */ var _tab5_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab5.page */ "./src/app/tab5/tab5.page.ts");







let Tab5PageModule = class Tab5PageModule {
};
Tab5PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _tab5_routing_module__WEBPACK_IMPORTED_MODULE_5__["Tab5PageRoutingModule"]
        ],
        declarations: [_tab5_page__WEBPACK_IMPORTED_MODULE_6__["Tab5Page"]]
    })
], Tab5PageModule);



/***/ }),

/***/ "./src/app/tab5/tab5.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab5/tab5.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".blog_card {\n  box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.16);\n  margin-bottom: 25px;\n}\n\n.blog_card_img {\n  background-size: cover !important;\n  background-repeat: no-repeat !important;\n  background-position: center !important;\n  height: 140px;\n  width: 100%;\n}\n\n.blog_card_body {\n  margin-top: 14px;\n  padding: 10px 10px 17px;\n}\n\n.blog_card_body h2 {\n  margin-top: 10px;\n  font-family: \"Gotham-Bold\";\n  font-size: 15px;\n}\n\n.blog_card_body p {\n  line-height: 19px;\n  font-size: 14px;\n  font-family: \"Lora-Regular\";\n}\n\nspan.date {\n  margin-top: 8px;\n  display: block;\n  font-size: 10px;\n  margin-bottom: 10px;\n  font-family: \"Gotham-Book\";\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiNS90YWI1LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdDLCtDQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFDQTtFQUNDLGlDQUFBO0VBQ0EsdUNBQUE7RUFDQSxzQ0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0FBRUQ7O0FBQUE7RUFDQyxnQkFBQTtFQUNBLHVCQUFBO0FBR0Q7O0FBREE7RUFDQyxnQkFBQTtFQUNFLDBCQUFBO0VBQ0EsZUFBQTtBQUlIOztBQUZBO0VBQ0MsaUJBQUE7RUFDQSxlQUFBO0VBQ0csMkJBQUE7QUFLSjs7QUFIQTtFQUNDLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0UsMEJBQUE7QUFNSCIsImZpbGUiOiJzcmMvYXBwL3RhYjUvdGFiNS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmxvZ19jYXJke1xyXG5cdC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDVweCA1cHggMHB4IHJnYmEoMCwwLDAsMC4xNik7XHJcblx0LW1vei1ib3gtc2hhZG93OiAwcHggNXB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjE2KTtcclxuXHRib3gtc2hhZG93OiAwcHggNXB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjE2KTtcclxuXHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG59XHJcbi5ibG9nX2NhcmRfaW1ne1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY292ZXIgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0ICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcblx0aGVpZ2h0OiAxNDBweDtcclxuXHR3aWR0aDogMTAwJTtcclxufVxyXG4uYmxvZ19jYXJkX2JvZHl7XHJcblx0bWFyZ2luLXRvcDogMTRweDtcclxuXHRwYWRkaW5nOiAxMHB4IDEwcHggMTdweDtcclxufVxyXG4uYmxvZ19jYXJkX2JvZHkgaDJ7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxuICAgZm9udC1mYW1pbHk6ICdHb3RoYW0tQm9sZCc7XHJcbiAgIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4uYmxvZ19jYXJkX2JvZHkgcHtcclxuXHRsaW5lLWhlaWdodDogMTlweDtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcbiAgIFx0Zm9udC1mYW1pbHk6ICdMb3JhLVJlZ3VsYXInO1xyXG59XHJcbnNwYW4uZGF0ZXtcclxuXHRtYXJnaW4tdG9wOiA4cHg7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0Zm9udC1zaXplOiAxMHB4O1xyXG5cdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgIGZvbnQtZmFtaWx5OiAnR290aGFtLUJvb2snO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/tab5/tab5.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab5/tab5.page.ts ***!
  \***********************************/
/*! exports provided: Tab5Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab5Page", function() { return Tab5Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _url_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url.service */ "./src/app/url.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




//import { HttpClient } from '@angular/common/http';



let Tab5Page = class Tab5Page {
    constructor(router, http, url, loadingController, sanitizer) {
        this.router = router;
        this.http = http;
        this.url = url;
        this.loadingController = loadingController;
        this.sanitizer = sanitizer;
    }
    go(Event) {
        this.router.navigate(['event-details'], { state: { data: Event } });
    }
    ngOnInit() {
        this.Get_EventData();
        //this.GetEventsPosts();
    }
    GetEventsPosts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message: 'Loading Data',
            });
            let url = this.url.event_api();
            let headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
            };
            this.http.post(url, {}, headers)
                .then(adata => {
                var data = JSON.parse(adata.data);
                localStorage.setItem('EventData', adata.data);
                this.LetestEventsDatas = data['item'];
                this.loading.dismiss();
            })
                .catch(error => {
                console.log(error);
                this.loading.dismiss();
            });
            this.loading.present();
        });
    }
    Get_EventData() {
        var DataObj = localStorage.getItem('EventData');
        var FinalDataObj = JSON.parse(DataObj);
        this.LetestEventsDatas = FinalDataObj['item'];
    }
    GetDateTime(d) {
        var dt = new Date(d);
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
        return result;
    }
    ;
    doRefresh(event) {
        console.log('Begin async operation');
        this.GetEventsPosts();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }
    getUrl(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
};
Tab5Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__["HTTP"] },
    { type: _url_service__WEBPACK_IMPORTED_MODULE_4__["UrlService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["DomSanitizer"] }
];
Tab5Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab5',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tab5.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab5/tab5.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tab5.page.scss */ "./src/app/tab5/tab5.page.scss")).default]
    })
], Tab5Page);



/***/ })

}]);
//# sourceMappingURL=tab5-tab5-module-es2015.js.map