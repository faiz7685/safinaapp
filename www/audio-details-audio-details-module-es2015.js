(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["audio-details-audio-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/audio-details/audio-details.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/audio-details/audio-details.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\" class=\"header_icons\" (click)=\"go()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        Safina Society Audio\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">     \r\n        <ion-icon name=\"chevron-back-outline\" style=\"color: #000 !important\"></ion-icon>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\t<div class=\"wrapper\">\r\n\t    <div class=\"blog_details_bg\" style=\"background: url('./../assets/images/audio.png');\">\r\n\t      <div class=\"dark_bg\"></div>\r\n\t      <div class=\"aduio_name\">\r\n\t      \t<h2>{{title}}</h2>\r\n\t      \t<ul>\r\n\t      \t\t<li>Season 3</li><li> | </li><li> {{GetDateTime(DateTime)}}</li>\r\n\t      \t</ul>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class=\"clearfix\"></div>\r\n\r\n\t    <ion-grid>\r\n\t    \t<ion-row>\r\n\t    \t\t<ion-col size=\"12\">\r\n\t    \t\t\t<div class=\"audio_play\">\r\n\t    \t\t\t\t<audio controls>\r\n\t\t\t\t\t\t  <source [src]=\"audiolink\" type=\"audio/mp3\">\r\n\t\t\t\t\t\t  Your browser does not support the audio element.\r\n\t\t\t\t\t\t</audio>\r\n\t    \t\t\t</div>\r\n\t    \t\t</ion-col>\r\n\t\t\t\t<ion-col size=\"12\">\r\n\t\t\t\t\t<div class=\"blog_details\">\r\n\t\t\t\t\t\t<span class=\"writer\">Description</span>\r\n\t\t\t\t\t\t<p>{{summary}}</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</ion-col>\r\n\t    \t</ion-row>\r\n\t    </ion-grid>\r\n\t</div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/audio-details/audio-details-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/audio-details/audio-details-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: AudioDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AudioDetailsPageRoutingModule", function() { return AudioDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _audio_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./audio-details.page */ "./src/app/audio-details/audio-details.page.ts");




const routes = [
    {
        path: '',
        component: _audio_details_page__WEBPACK_IMPORTED_MODULE_3__["AudioDetailsPage"]
    }
];
let AudioDetailsPageRoutingModule = class AudioDetailsPageRoutingModule {
};
AudioDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AudioDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/audio-details/audio-details.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/audio-details/audio-details.module.ts ***!
  \*******************************************************/
/*! exports provided: AudioDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AudioDetailsPageModule", function() { return AudioDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _audio_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./audio-details-routing.module */ "./src/app/audio-details/audio-details-routing.module.ts");
/* harmony import */ var _audio_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./audio-details.page */ "./src/app/audio-details/audio-details.page.ts");







let AudioDetailsPageModule = class AudioDetailsPageModule {
};
AudioDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _audio_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["AudioDetailsPageRoutingModule"]
        ],
        declarations: [_audio_details_page__WEBPACK_IMPORTED_MODULE_6__["AudioDetailsPage"]]
    })
], AudioDetailsPageModule);



/***/ }),

/***/ "./src/app/audio-details/audio-details.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/audio-details/audio-details.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".blog_details_bg {\n  background-repeat: no-repeat !important;\n  background-position: center !important;\n  background-size: cover !important;\n  height: 374px;\n  width: 100%;\n  position: relative;\n}\n\n.dark_bg {\n  background: rgba(0, 0, 0, 0.75);\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n\n.audio_play {\n  text-align: center;\n}\n\naudio::-webkit-media-controls-play-button,\naudio::-webkit-media-controls-panel {\n  background-color: #fff;\n  color: #000;\n}\n\n.blog_details {\n  margin-bottom: 30px;\n  font-family: \"Lora-Regular\";\n}\n\n.blog_details span {\n  margin-bottom: 16px;\n  margin-top: 5px;\n  display: block;\n}\n\n.blog_details h1 {\n  font-size: 18px;\n}\n\n.blog_details p {\n  font-size: 12px;\n  line-height: 19px;\n  margin-top: 16px;\n}\n\n.aduio_name ul {\n  padding: 0;\n  margin: 0;\n}\n\n.aduio_name ul li {\n  display: inline-block;\n  padding-right: 5px;\n  font-size: 12px;\n}\n\n.aduio_name {\n  position: absolute;\n  left: 20px;\n  right: 20px;\n  bottom: 32px;\n  max-width: 50%;\n  width: 100%;\n  color: #fff;\n}\n\n.aduio_name h2 {\n  font-size: 18px;\n  margin-bottom: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXVkaW8tZGV0YWlscy9hdWRpby1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHVDQUFBO0VBQ0Esc0NBQUE7RUFDQSxpQ0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFDRDs7QUFDQTtFQUNDLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBRUQ7O0FBQUE7RUFDSSxrQkFBQTtBQUdKOztBQURBOztFQUVDLHNCQUFBO0VBQ0EsV0FBQTtBQUlEOztBQURBO0VBQ0csbUJBQUE7RUFDQywyQkFBQTtBQUlKOztBQUZBO0VBQ0MsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUtEOztBQUhBO0VBQ0MsZUFBQTtBQU1EOztBQUpBO0VBQ0MsZUFBQTtFQUNHLGlCQUFBO0VBQ0gsZ0JBQUE7QUFPRDs7QUFMQTtFQUNDLFVBQUE7RUFDQSxTQUFBO0FBUUQ7O0FBTkE7RUFDQyxxQkFBQTtFQUNBLGtCQUFBO0VBQ0csZUFBQTtBQVNKOztBQVBBO0VBQ0Msa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUFVRDs7QUFSQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQVdKIiwiZmlsZSI6InNyYy9hcHAvYXVkaW8tZGV0YWlscy9hdWRpby1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ibG9nX2RldGFpbHNfYmd7XHJcblx0YmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdCAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY292ZXIgIWltcG9ydGFudDtcclxuXHRoZWlnaHQ6IDM3NHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uZGFya19iZ3tcclxuXHRiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDAuNzUpO1xyXG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHR0b3A6IDA7XHJcblx0bGVmdDogMDtcclxuXHRyaWdodDogMDtcclxuXHRib3R0b206IDA7XHJcbn1cclxuLmF1ZGlvX3BsYXkge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbmF1ZGlvOjotd2Via2l0LW1lZGlhLWNvbnRyb2xzLXBsYXktYnV0dG9uLFxyXG5hdWRpbzo6LXdlYmtpdC1tZWRpYS1jb250cm9scy1wYW5lbCB7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuXHRjb2xvcjogIzAwMDtcclxufVxyXG5cclxuLmJsb2dfZGV0YWlscyB7XHJcbiAgXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgZm9udC1mYW1pbHk6ICdMb3JhLVJlZ3VsYXInO1xyXG59XHJcbi5ibG9nX2RldGFpbHMgc3BhbntcclxuXHRtYXJnaW4tYm90dG9tOiAxNnB4O1xyXG5cdG1hcmdpbi10b3A6IDVweDtcclxuXHRkaXNwbGF5OiBibG9jaztcclxufVxyXG4uYmxvZ19kZXRhaWxzIGgxe1xyXG5cdGZvbnQtc2l6ZTogMThweDtcclxufVxyXG4uYmxvZ19kZXRhaWxzIHB7ICAgIFxyXG5cdGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG5cdG1hcmdpbi10b3A6IDE2cHg7XHJcbn1cclxuLmFkdWlvX25hbWUgdWx7XHJcblx0cGFkZGluZzogMDtcclxuXHRtYXJnaW46IDA7XHJcbn1cclxuLmFkdWlvX25hbWUgdWwgbGl7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrOyAgICBcclxuXHRwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuLmFkdWlvX25hbWV7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdGxlZnQ6IDIwcHg7XHJcblx0cmlnaHQ6IDIwcHg7XHJcblx0Ym90dG9tOiAzMnB4O1xyXG5cdG1heC13aWR0aDogNTAlO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGNvbG9yOiAjZmZmO1xyXG59XHJcbi5hZHVpb19uYW1lIGgyIHtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDhweFxyXG59Il19 */");

/***/ }),

/***/ "./src/app/audio-details/audio-details.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/audio-details/audio-details.page.ts ***!
  \*****************************************************/
/*! exports provided: AudioDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AudioDetailsPage", function() { return AudioDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




let AudioDetailsPage = class AudioDetailsPage {
    //sanitizer: any;
    constructor(router, sanitizer) {
        this.router = router;
        this.sanitizer = sanitizer;
    }
    go() {
        this.router.navigate(['tabs/Podcast']);
    }
    ngOnInit() {
        var PodcastData = this.router.getCurrentNavigation().extras.state;
        var FinalData = PodcastData.data;
        //console.log(FinalData);
        this.title = FinalData.title;
        this.DateTime = FinalData.pubdate;
        this.audiolink = this.sanitizer.bypassSecurityTrustResourceUrl(FinalData.audio_link);
        this.summary = FinalData.summary;
        this.author = FinalData.author;
    }
    // getSafeUrl(url) {
    // 	//this.finalUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);		
    // }
    GetDateTime(d) {
        var dt = new Date(d);
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
        return result;
    }
    ;
};
AudioDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
];
AudioDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-audio-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./audio-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/audio-details/audio-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./audio-details.page.scss */ "./src/app/audio-details/audio-details.page.scss")).default]
    })
], AudioDetailsPage);



/***/ })

}]);
//# sourceMappingURL=audio-details-audio-details-module-es2015.js.map