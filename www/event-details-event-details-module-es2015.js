(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["event-details-event-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/event-details/event-details.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/event-details/event-details.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\" class=\"header_icons\" (click)=\"go()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        ANNOUNCEMENTS\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">     \r\n        <ion-icon name=\"chevron-back-outline\" style=\"color: #000 !important\"></ion-icon>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\t<div class=\"wrapper\">\r\n\t\t<div class=\"blog_details_bg\" style=\"background: url('{{img}}');\"></div>\r\n\t\t<div class=\"clearfix\"></div>\r\n\t\t<ion-grid>\r\n\t\t\t<ion-row>\r\n\t\t\t\t<ion-col size=\"12\">\r\n\t\t\t\t\t<div class=\"blog_details\">\r\n\t\t\t\t\t\t<h1>{{title}}</h1>\r\n\t\t\t\t\t\t<span class=\"writer\">{{GetDateTime(pubdate)}}</span>\r\n\t\t\t\t\t\t<div [innerHTML]=\"description\"></div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</ion-col>\r\n\t\t\t</ion-row>\r\n\t\t</ion-grid>\r\n\t</div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/event-details/event-details-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/event-details/event-details-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: EventDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventDetailsPageRoutingModule", function() { return EventDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _event_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./event-details.page */ "./src/app/event-details/event-details.page.ts");




const routes = [
    {
        path: '',
        component: _event_details_page__WEBPACK_IMPORTED_MODULE_3__["EventDetailsPage"]
    }
];
let EventDetailsPageRoutingModule = class EventDetailsPageRoutingModule {
};
EventDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EventDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/event-details/event-details.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/event-details/event-details.module.ts ***!
  \*******************************************************/
/*! exports provided: EventDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventDetailsPageModule", function() { return EventDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _event_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./event-details-routing.module */ "./src/app/event-details/event-details-routing.module.ts");
/* harmony import */ var _event_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./event-details.page */ "./src/app/event-details/event-details.page.ts");







let EventDetailsPageModule = class EventDetailsPageModule {
};
EventDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _event_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["EventDetailsPageRoutingModule"]
        ],
        declarations: [_event_details_page__WEBPACK_IMPORTED_MODULE_6__["EventDetailsPage"]]
    })
], EventDetailsPageModule);



/***/ }),

/***/ "./src/app/event-details/event-details.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/event-details/event-details.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".blog_details_bg {\n  background-repeat: no-repeat !important;\n  background-position: center !important;\n  background-size: cover !important;\n  height: 220px;\n  width: 100%;\n}\n\n.blog_details {\n  margin-bottom: 50px;\n  margin-top: 20px;\n}\n\n.blog_details h1 {\n  font-size: 18px;\n}\n\nspan.writer {\n  font-size: 12px;\n  margin-top: 8px;\n  display: block;\n}\n\n.blog_details p {\n  line-height: 23px;\n  margin-top: 32px;\n}\n\nspan.underline {\n  display: block;\n  text-decoration: underline;\n  margin: 10px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZlbnQtZGV0YWlscy9ldmVudC1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHVDQUFBO0VBQ0Esc0NBQUE7RUFDQSxpQ0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0FBQ0Q7O0FBQ0E7RUFDQyxtQkFBQTtFQUNBLGdCQUFBO0FBRUQ7O0FBQUE7RUFDQyxlQUFBO0FBR0Q7O0FBREE7RUFDQyxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFJRDs7QUFGQTtFQUNDLGlCQUFBO0VBQ0EsZ0JBQUE7QUFLRDs7QUFIQTtFQUNDLGNBQUE7RUFDQSwwQkFBQTtFQUNBLGNBQUE7QUFNRCIsImZpbGUiOiJzcmMvYXBwL2V2ZW50LWRldGFpbHMvZXZlbnQtZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmxvZ19kZXRhaWxzX2Jne1xyXG5cdGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvdmVyICFpbXBvcnRhbnQ7XHJcblx0aGVpZ2h0OiAyMjBweDtcclxuXHR3aWR0aDogMTAwJTtcclxufVxyXG4uYmxvZ19kZXRhaWxze1xyXG5cdG1hcmdpbi1ib3R0b206IDUwcHg7XHJcblx0bWFyZ2luLXRvcDogMjBweDtcclxufVxyXG4uYmxvZ19kZXRhaWxzIGgxe1xyXG5cdGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5zcGFuLndyaXRlcntcclxuXHRmb250LXNpemU6IDEycHg7XHJcblx0bWFyZ2luLXRvcDogOHB4O1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbi5ibG9nX2RldGFpbHMgcHtcclxuXHRsaW5lLWhlaWdodDogMjNweDtcclxuXHRtYXJnaW4tdG9wOiAzMnB4O1xyXG59XHJcbnNwYW4udW5kZXJsaW5le1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG5cdHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG5cdG1hcmdpbjogMTBweCAwO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/event-details/event-details.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/event-details/event-details.page.ts ***!
  \*****************************************************/
/*! exports provided: EventDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventDetailsPage", function() { return EventDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




let EventDetailsPage = class EventDetailsPage {
    constructor(router, sanitizer) {
        this.router = router;
        this.sanitizer = sanitizer;
    }
    go() {
        this.router.navigate(['tabs/Event']);
    }
    ngOnInit() {
        var EventData = this.router.getCurrentNavigation().extras.state;
        var FinalData = EventData.data;
        this.title = FinalData.title;
        this.pubdate = FinalData.pubdate;
        this.description = FinalData.content;
        this.img = FinalData.image;
    }
    GetDateTime(d) {
        var dt = new Date(d);
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
        return result;
    }
    ;
    getUrl(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
};
EventDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
];
EventDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-event-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./event-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/event-details/event-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./event-details.page.scss */ "./src/app/event-details/event-details.page.scss")).default]
    })
], EventDetailsPage);



/***/ })

}]);
//# sourceMappingURL=event-details-event-details-module-es2015.js.map