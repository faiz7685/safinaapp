(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab4-tab4-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <!-- <img src=\"../../assets/images/logo.png\" alt=\"logo\" class=\"header_logo\"> -->\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        SAFINA SOCIETY VIDEOS\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">\r\n        <!-- <ion-icon name=\"information-circle-outline\" size=\"medium\"></ion-icon> -->\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n  <div class=\"wrapper\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <div class=\"search_btn\">\r\n              <ion-input type=\"search\" placeholder=\"Search\" (keyup)=\"searchText(Search.value)\" #Search></ion-input>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\" *ngFor=\"let VideoData of LetestVideoDatas\">\r\n          <div class=\"blog_card\">\r\n            <ion-row>\r\n              <ion-col size=\"2\">\r\n                <div class=\"blog_card_img\" style=\"background: url('{{VideoData.snippet.thumbnails.default.url}}');\"></div>                \r\n              </ion-col>\r\n              <ion-col size=\"10\">\r\n                <div class=\"blog_card_body\" (click)=\"go(VideoData)\">\r\n                  <h2>{{VideoData.snippet.title}}</h2>\r\n                  <span class=\"date\">{{GetDateTime(VideoData.snippet.publishTime)}}</span>\r\n                  <p>{{VideoData.snippet.description}}</p>\r\n                </div>                \r\n              </ion-col>              \r\n            </ion-row>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\" *ngIf=\"token!=''\">\r\n          <ion-button expand=\"block\" (click)=\"GetMoreVideos()\">Load More</ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/tab4/tab4-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tab4/tab4-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab4PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4PageRoutingModule", function() { return Tab4PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tab4_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab4.page */ "./src/app/tab4/tab4.page.ts");




const routes = [
    {
        path: '',
        component: _tab4_page__WEBPACK_IMPORTED_MODULE_3__["Tab4Page"]
    }
];
let Tab4PageRoutingModule = class Tab4PageRoutingModule {
};
Tab4PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Tab4PageRoutingModule);



/***/ }),

/***/ "./src/app/tab4/tab4.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.module.ts ***!
  \*************************************/
/*! exports provided: Tab4PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4PageModule", function() { return Tab4PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _tab4_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab4-routing.module */ "./src/app/tab4/tab4-routing.module.ts");
/* harmony import */ var _tab4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab4.page */ "./src/app/tab4/tab4.page.ts");







let Tab4PageModule = class Tab4PageModule {
};
Tab4PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _tab4_routing_module__WEBPACK_IMPORTED_MODULE_5__["Tab4PageRoutingModule"]
        ],
        declarations: [_tab4_page__WEBPACK_IMPORTED_MODULE_6__["Tab4Page"]]
    })
], Tab4PageModule);



/***/ }),

/***/ "./src/app/tab4/tab4.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".blog_card {\n  box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.13);\n  margin-bottom: 25px;\n}\n\n.blog_card_img {\n  background-size: cover !important;\n  background-repeat: no-repeat !important;\n  background-position: center !important;\n  height: 45px;\n  width: 100%;\n}\n\n.blog_card_body {\n  margin-top: 0px;\n  padding: 0px 0px 17px;\n}\n\nspan.category {\n  background: #3A0A46;\n  display: block;\n  font-size: 10px;\n  max-width: 40%;\n  color: #fff;\n  text-transform: uppercase;\n  font-family: \"Gotham-Medium\";\n  text-align: center;\n  padding: 3px;\n}\n\n.blog_card_body h2 {\n  font-family: \"Gotham-Bold\";\n  font-size: 15px;\n}\n\n.blog_card_body p {\n  line-height: 16px;\n  font-size: 12px;\n  font-family: \"Lora-Regular\";\n  margin-top: 0px;\n}\n\nspan.date {\n  margin-top: 8px;\n  display: block;\n  font-size: 10px;\n  margin-bottom: 7px;\n  font-family: \"Gotham-Book\";\n}\n\n.search_btn {\n  margin-bottom: 20px;\n  margin-top: 10px;\n}\n\nion-input.sc-ion-input-md-h.sc-ion-input-md-s.md.hydrated {\n  box-shadow: 1px 2px 4px 1px rgba(213, 213, 213, 0.5);\n  border: 1px solid #d5d5d5;\n  font-family: \"Gotham-Medium\";\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiNC90YWI0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdDLCtDQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFDQTtFQUNDLGlDQUFBO0VBQ0EsdUNBQUE7RUFDQSxzQ0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBRUQ7O0FBQUE7RUFDQyxlQUFBO0VBQ0EscUJBQUE7QUFHRDs7QUFEQTtFQUNDLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0UsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFJSDs7QUFGQTtFQUNHLDBCQUFBO0VBQ0EsZUFBQTtBQUtIOztBQUhBO0VBQ0MsaUJBQUE7RUFDQSxlQUFBO0VBQ0csMkJBQUE7RUFDQSxlQUFBO0FBTUo7O0FBSkE7RUFDQyxlQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNFLDBCQUFBO0FBT0g7O0FBTEE7RUFDQyxtQkFBQTtFQUNBLGdCQUFBO0FBUUQ7O0FBTkE7RUFDSSxvREFBQTtFQUNBLHlCQUFBO0VBQ0QsNEJBQUE7QUFTSCIsImZpbGUiOiJzcmMvYXBwL3RhYjQvdGFiNC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmxvZ19jYXJke1xyXG5cdC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDVweCA1cHggMHB4IHJnYmEoMCwwLDAsMC4xMyk7XHJcblx0LW1vei1ib3gtc2hhZG93OiAwcHggNXB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjEzKTtcclxuXHRib3gtc2hhZG93OiAwcHggNXB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjEzKTtcclxuXHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG59XHJcbi5ibG9nX2NhcmRfaW1ne1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY292ZXIgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0ICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcblx0aGVpZ2h0OiA0NXB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcbi5ibG9nX2NhcmRfYm9keXtcclxuXHRtYXJnaW4tdG9wOiAwcHg7XHJcblx0cGFkZGluZzogMHB4IDBweCAxN3B4O1xyXG59XHJcbnNwYW4uY2F0ZWdvcnl7XHJcblx0YmFja2dyb3VuZDogIzNBMEE0NjtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRmb250LXNpemU6IDEwcHg7XHJcblx0bWF4LXdpZHRoOiA0MCU7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgZm9udC1mYW1pbHk6ICdHb3RoYW0tTWVkaXVtJztcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICBwYWRkaW5nOiAzcHg7XHJcbn1cclxuLmJsb2dfY2FyZF9ib2R5IGgye1xyXG4gICBmb250LWZhbWlseTogJ0dvdGhhbS1Cb2xkJztcclxuICAgZm9udC1zaXplOiAxNXB4O1xyXG59XHJcbi5ibG9nX2NhcmRfYm9keSBwe1xyXG5cdGxpbmUtaGVpZ2h0OiAxNnB4O1xyXG5cdGZvbnQtc2l6ZTogMTJweDtcclxuICAgXHRmb250LWZhbWlseTogJ0xvcmEtUmVndWxhcic7XHJcbiAgIFx0bWFyZ2luLXRvcDogMHB4O1xyXG59XHJcbnNwYW4uZGF0ZXtcclxuXHRtYXJnaW4tdG9wOiA4cHg7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0Zm9udC1zaXplOiAxMHB4O1xyXG5cdG1hcmdpbi1ib3R0b206IDdweDtcclxuICAgZm9udC1mYW1pbHk6ICdHb3RoYW0tQm9vayc7XHJcbn1cclxuLnNlYXJjaF9idG57XHJcblx0bWFyZ2luLWJvdHRvbTogMjBweDtcclxuXHRtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcbmlvbi1pbnB1dC5zYy1pb24taW5wdXQtbWQtaC5zYy1pb24taW5wdXQtbWQtcy5tZC5oeWRyYXRlZCB7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMnB4IDRweCAxcHggcmdiYSgyMTMsIDIxMywgMjEzLCAwLjUpO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q1ZDVkNTtcclxuICAgZm9udC1mYW1pbHk6ICdHb3RoYW0tTWVkaXVtJztcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/tab4/tab4.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab4/tab4.page.ts ***!
  \***********************************/
/*! exports provided: Tab4Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4Page", function() { return Tab4Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _url_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url.service */ "./src/app/url.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _loder_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../loder.service */ "./src/app/loder.service.ts");







let Tab4Page = class Tab4Page {
    constructor(router, netiveHttp, url, loadingController, ionLoader) {
        this.router = router;
        this.netiveHttp = netiveHttp;
        this.url = url;
        this.loadingController = loadingController;
        this.ionLoader = ionLoader;
        this.token = '';
    }
    go(VideoData) {
        this.router.navigate(['video-details'], { state: { data: VideoData } });
    }
    ngOnInit() {
        //this.GetVideoPosts();
        this.Get_VideoData();
    }
    GetVideoPosts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message: 'Loading Data',
            });
            let url = this.url.video_api();
            let headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
            };
            this.netiveHttp.post(url, {}, headers)
                .then(adata => {
                var data = JSON.parse(adata.data);
                localStorage.setItem('VideoData', adata.data);
                this.LetestVideoDatas = data['details'].results;
                this.LetestVideoDatasNew = data['details'].results;
                this.token = data['details'].info.nextPageToken;
                this.loading.dismiss();
            })
                .catch(error => {
                console.log(error);
                this.loading.dismiss();
            });
            this.loading.present();
        });
    }
    Get_VideoData() {
        var DataObj = localStorage.getItem('VideoData');
        var FinalDataObj = JSON.parse(DataObj);
        this.LetestVideoDatas = FinalDataObj['details'].results;
        this.LetestVideoDatasNew = FinalDataObj['details'].results;
        this.token = FinalDataObj['details'].info.nextPageToken;
    }
    GetMoreVideos() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message: 'Loading Data',
            });
            let url = this.url.video_api();
            let headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
            };
            this.netiveHttp.post(url, { 'next_page_token': this.token }, headers)
                .then(adata => {
                var data = JSON.parse(adata.data);
                var finalData = data;
                var item = finalData['details'].results;
                this.token = finalData['details'].info.nextPageToken;
                item.forEach(element => {
                    this.LetestVideoDatas.push(element);
                    this.LetestVideoDatasNew.push(element);
                });
                console.log(this.LetestVideoDatas);
                this.loading.dismiss();
            })
                .catch(error => {
                console.log(error);
                this.loading.dismiss();
            });
            this.loading.present();
        });
    }
    GetDateTime(d) {
        var dt = new Date(d);
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
        return result;
    }
    ;
    doRefresh(event) {
        console.log('Begin async operation');
        this.GetVideoPosts();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }
    searchText(e) {
        this.LetestVideoDatas = this.ionLoader.filterItemsforYoutube(this.LetestVideoDatasNew, e);
    }
};
Tab4Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__["HTTP"] },
    { type: _url_service__WEBPACK_IMPORTED_MODULE_4__["UrlService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _loder_service__WEBPACK_IMPORTED_MODULE_6__["LoderService"] }
];
Tab4Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab4',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tab4.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tab4.page.scss */ "./src/app/tab4/tab4.page.scss")).default]
    })
], Tab4Page);



/***/ })

}]);
//# sourceMappingURL=tab4-tab4-module-es2015.js.map