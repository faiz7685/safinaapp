(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <img src=\"../../assets/images/logo.png\" alt=\"logo\" class=\"header_logo\">\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        Safina Society\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\" (click)=\"go()\">\r\n        <ion-icon name=\"information-circle-outline\" size=\"medium\"></ion-icon>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n  <div class=\"wrapper\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_img\" style=\"background: url('{{LetestBlogimg}}') no-repeat center center fixed;\"></div>\r\n            <!-- <div class=\"blog_card_img\" >\r\n              <img [src]=\"LetestBlogimg\">\r\n            </div> -->\r\n            <div class=\"clearfix\"></div>\r\n            <div class=\"blog_card_body\" (click)=\"GotoBlog()\">\r\n              <span class=\"category\"> NEW BLOG</span>\r\n              <h2>{{LetestBlogTitle}}</h2>\r\n              <span class=\"date\">{{GetDateTime(LetestBlogTime)}}</span>\r\n              <p>{{LetestBlogDesc}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_img\" style=\"background: url('{{LetestPodcastImg}}');\"></div>\r\n            <div class=\"clearfix\"></div>\r\n            <div class=\"blog_card_body\" (click)=\"GotoPodcast()\">\r\n              <span class=\"category\"> NEW PODCAST</span>\r\n              <h2>{{LetestPodcastTitle}}</h2>\r\n              <span class=\"date\">{{GetDateTime(LetestPodcastTime)}}</span>\r\n              <p>{{LetestPodcastDesc}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_img\" style=\"background: url('{{LetestVideoImg}}');\"></div>\r\n            <div class=\"clearfix\"></div>\r\n            <div class=\"blog_card_body\" (click)=\"GotoVedio()\">\r\n              <span class=\"category\"> NEW VIDEO</span>\r\n              <h2>{{LetestVideoTitle}}</h2>\r\n              <span class=\"date\">{{GetDateTime(LetestVideoTime)}}</span>\r\n              <p>{{LetestVideoDesc}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div class=\"blog_card\" (click)=\"GotoEvents()\">\r\n            <div class=\"blog_card_img\" style=\"background: url('{{LetestEventImg}}');\"></div>\r\n            <div class=\"clearfix\"></div>\r\n            <div class=\"blog_card_body\">\r\n              <span class=\"category\"> New EVENT</span>\r\n              <h2>{{LetestEventTitle}}</h2>\r\n              <span class=\"date\">{{GetDateTime(LetestEventTime)}}</span>\r\n              <p>{{LetestEventDesc}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/tab1/tab1-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tab1/tab1-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function() { return Tab1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"],
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ "./src/app/tab1/tab1.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tab1-routing.module */ "./src/app/tab1/tab1-routing.module.ts");








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab1PageRoutingModule"]
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ }),

/***/ "./src/app/tab1/tab1.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".blog_card {\n  box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.16);\n  margin-bottom: 25px;\n}\n\n.blog_card_img {\n  background-size: cover !important;\n  background-repeat: no-repeat !important;\n  background-position: center !important;\n  height: 140px;\n  width: 100%;\n}\n\n.blog_card_body {\n  margin-top: 14px;\n  padding: 10px 10px 17px;\n}\n\nspan.category {\n  background: #3A0A46;\n  font-size: 10px;\n  color: #fff;\n  text-transform: uppercase;\n  font-family: \"Gotham-Medium\";\n  text-align: center;\n  padding: 3px 5px;\n}\n\n.blog_card_body h2 {\n  margin-top: 10px;\n  font-family: \"Gotham-Bold\";\n  font-size: 15px;\n}\n\n.blog_card_body p {\n  line-height: 19px;\n  font-size: 14px;\n  font-family: \"Lora-Regular\";\n}\n\nspan.date {\n  margin-top: 8px;\n  display: block;\n  font-size: 10px;\n  margin-bottom: 10px;\n  font-family: \"Gotham-Book\";\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiMS90YWIxLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdDLCtDQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFDQTtFQUNDLGlDQUFBO0VBQ0EsdUNBQUE7RUFDQSxzQ0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0FBRUQ7O0FBQUE7RUFDQyxnQkFBQTtFQUNBLHVCQUFBO0FBR0Q7O0FBREE7RUFDQyxtQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDRSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFJSDs7QUFGQTtFQUNDLGdCQUFBO0VBQ0UsMEJBQUE7RUFDQSxlQUFBO0FBS0g7O0FBSEE7RUFDQyxpQkFBQTtFQUNBLGVBQUE7RUFDRywyQkFBQTtBQU1KOztBQUpBO0VBQ0MsZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDRSwwQkFBQTtBQU9IIiwiZmlsZSI6InNyYy9hcHAvdGFiMS90YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ibG9nX2NhcmR7XHJcblx0LXdlYmtpdC1ib3gtc2hhZG93OiAwcHggNXB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjE2KTtcclxuXHQtbW96LWJveC1zaGFkb3c6IDBweCA1cHggNXB4IDBweCByZ2JhKDAsMCwwLDAuMTYpO1xyXG5cdGJveC1zaGFkb3c6IDBweCA1cHggNXB4IDBweCByZ2JhKDAsMCwwLDAuMTYpO1xyXG5cdG1hcmdpbi1ib3R0b206IDI1cHg7XHJcbn1cclxuLmJsb2dfY2FyZF9pbWd7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb3ZlciAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgIWltcG9ydGFudDtcclxuXHRoZWlnaHQ6IDE0MHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcbi5ibG9nX2NhcmRfYm9keXtcclxuXHRtYXJnaW4tdG9wOiAxNHB4O1xyXG5cdHBhZGRpbmc6IDEwcHggMTBweCAxN3B4O1xyXG59XHJcbnNwYW4uY2F0ZWdvcnl7XHJcblx0YmFja2dyb3VuZDogIzNBMEE0NjtcclxuXHRmb250LXNpemU6IDEwcHg7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgZm9udC1mYW1pbHk6ICdHb3RoYW0tTWVkaXVtJztcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICBwYWRkaW5nOiAzcHggNXB4O1xyXG59XHJcbi5ibG9nX2NhcmRfYm9keSBoMntcclxuXHRtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICBmb250LWZhbWlseTogJ0dvdGhhbS1Cb2xkJztcclxuICAgZm9udC1zaXplOiAxNXB4O1xyXG59XHJcbi5ibG9nX2NhcmRfYm9keSBwe1xyXG5cdGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuICAgXHRmb250LWZhbWlseTogJ0xvcmEtUmVndWxhcic7XHJcbn1cclxuc3Bhbi5kYXRle1xyXG5cdG1hcmdpbi10b3A6IDhweDtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRmb250LXNpemU6IDEwcHg7XHJcblx0bWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgZm9udC1mYW1pbHk6ICdHb3RoYW0tQm9vayc7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/tab1/tab1.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _url_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url.service */ "./src/app/url.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");








let Tab1Page = class Tab1Page {
    constructor(router, netiveHttp, url, toastr, loadingController, sanitizer) {
        this.router = router;
        this.netiveHttp = netiveHttp;
        this.url = url;
        this.toastr = toastr;
        this.loadingController = loadingController;
        this.sanitizer = sanitizer;
    }
    ngOnInit() {
        this.GetBlogPosts();
        this.GetPodcastPosts();
        this.GetVideoPosts();
        this.GetEventsPosts();
    }
    GetBlogPosts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message: 'Loading Data',
            });
            let url = this.url.blog_feed_api();
            let headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
            };
            this.netiveHttp.post(url, {}, headers)
                .then(adata => {
                var data = JSON.parse(adata.data);
                localStorage.setItem('BlogData', adata.data);
                this.GotoBlogData = data['item'][0];
                console.log(data['item'][0].image);
                this.LetestBlogTitle = data['item'][0].title;
                this.LetestBlogTime = data['item'][0].pubdate;
                this.LetestBlogimg = data['item'][0].image;
                this.LetestBlogDesc = data['item'][0].description;
                this.loading.dismiss();
            })
                .catch(error => {
                console.log(error);
                this.loading.dismiss();
            });
            this.loading.present();
        });
    }
    GotoBlog() {
        this.router.navigate(['blog-details'], { state: { data: this.GotoBlogData } });
    }
    GetVideoPosts() {
        let url = this.url.video_api();
        let headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
        };
        this.netiveHttp.post(url, {}, headers)
            .then(adata => {
            var data = JSON.parse(adata.data);
            localStorage.setItem('VideoData', adata.data);
            this.GotoVedioData = data['details'].results[0];
            this.LetestVideoTitle = data['details'].results[0].snippet.title;
            this.LetestVideoTime = data['details'].results[0].snippet.publishTime;
            this.LetestVideoImg = data['details'].results[0].snippet.thumbnails.default.url;
            this.LetestVideoDesc = data['details'].results[0].snippet.description;
        })
            .catch(error => {
            console.log(error);
        });
    }
    GotoVedio() {
        this.router.navigate(['video-details'], { state: { data: this.GotoVedioData } });
    }
    GetEventsPosts() {
        let url = this.url.event_api();
        let headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
        };
        this.netiveHttp.post(url, {}, headers)
            .then(adata => {
            var data = JSON.parse(adata.data);
            localStorage.setItem('EventData', adata.data);
            this.LetestEventsDatas = data['item'][0];
            this.LetestEventTitle = data['item'][0].title;
            this.LetestEventTime = data['item'][0].pubdate;
            this.LetestEventImg = data['item'][0].image;
            this.LetestEventDesc = data['item'][0].description;
        })
            .catch(error => {
            console.log(error);
        });
    }
    GotoEvents() {
        this.router.navigate(['event-details'], { state: { data: this.LetestEventsDatas } });
    }
    GetPodcastPosts() {
        let url = this.url.Podcast_api();
        let headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
        };
        this.netiveHttp.post(url, {}, headers)
            .then(adata => {
            var data = JSON.parse(adata.data);
            localStorage.setItem('PodcastData', adata.data);
            this.LetestPodcastDatas = data['item'][0];
            this.LetestPodcastTitle = data['item'][0].title;
            this.LetestPodcastTime = data['item'][0].pubdate;
            this.LetestPodcastImg = data['item'][0].banner;
            this.LetestPodcastDesc = data['item'][0].description;
        })
            .catch(error => {
            console.log(error);
        });
    }
    GotoPodcast() {
        this.router.navigate(['audio-details'], { state: { data: this.LetestPodcastDatas } });
    }
    go() {
        this.router.navigate(['about-us']);
    }
    doRefresh(event) {
        console.log('Begin async operation');
        this.GetBlogPosts();
        this.GetPodcastPosts();
        this.GetVideoPosts();
        this.GetEventsPosts();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }
    GetDateTime(d) {
        var dt = new Date(d);
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
        return result;
    }
    ;
    getUrl(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
};
Tab1Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__["HTTP"] },
    { type: _url_service__WEBPACK_IMPORTED_MODULE_4__["UrlService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["DomSanitizer"] }
];
Tab1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab1',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tab1.page.scss */ "./src/app/tab1/tab1.page.scss")).default]
    })
], Tab1Page);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map