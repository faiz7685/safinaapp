(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["video-details-video-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/video-details/video-details.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/video-details/video-details.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\" class=\"header_icons\" (click)=\"go()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        SAFINA SOCIETY VIDEOS\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">     \r\n        <ion-icon name=\"chevron-back-outline\" style=\"color: #000 !important\"></ion-icon>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n  <div class=\"wrapper\">\r\n    <div class=\"blog_details_bg\" >\r\n      <!-- style=\"background: url('./../assets/images/video.png');\" -->\r\n      <iframe id=\"youtube_ifream\" width=\"100%\" height=\"250px\" [src]=\"GetUrl(embedHtml)\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n      <!-- <video [src]=\"GetUrl(embedHtml)\"></video> -->\r\n      <!-- <div class=\"dark_bg\"></div>\r\n      <div class=\"icon_play\">\r\n        \r\n        <ion-icon name=\"play\" ></ion-icon>\r\n      </div> -->\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <div class=\"blog_details\">\r\n            <h1>{{title}}</h1>\r\n            <span class=\"writer\">{{viewCount}} views · {{GetDateTime(datetime)}}</span>\r\n            <p>Description \r\n            <br><br>\r\n            {{description}}</p>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>  \r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/video-details/video-details-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/video-details/video-details-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: VideoDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoDetailsPageRoutingModule", function() { return VideoDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _video_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./video-details.page */ "./src/app/video-details/video-details.page.ts");




const routes = [
    {
        path: '',
        component: _video_details_page__WEBPACK_IMPORTED_MODULE_3__["VideoDetailsPage"]
    }
];
let VideoDetailsPageRoutingModule = class VideoDetailsPageRoutingModule {
};
VideoDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], VideoDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/video-details/video-details.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/video-details/video-details.module.ts ***!
  \*******************************************************/
/*! exports provided: VideoDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoDetailsPageModule", function() { return VideoDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _video_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./video-details-routing.module */ "./src/app/video-details/video-details-routing.module.ts");
/* harmony import */ var _video_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./video-details.page */ "./src/app/video-details/video-details.page.ts");







let VideoDetailsPageModule = class VideoDetailsPageModule {
};
VideoDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _video_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["VideoDetailsPageRoutingModule"]
        ],
        declarations: [_video_details_page__WEBPACK_IMPORTED_MODULE_6__["VideoDetailsPage"]]
    })
], VideoDetailsPageModule);



/***/ }),

/***/ "./src/app/video-details/video-details.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/video-details/video-details.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".blog_details_bg {\n  background-repeat: no-repeat !important;\n  background-position: center !important;\n  background-size: cover !important;\n  width: 100%;\n  position: relative;\n}\n\n.button_group {\n  max-width: 50px;\n  margin: 10px auto 0;\n  display: block;\n  overflow: hidden;\n}\n\n.back {\n  float: left;\n  font-size: 16px;\n}\n\n.for {\n  float: right;\n  font-size: 16px;\n}\n\n.dark_bg {\n  background: rgba(0, 0, 0, 0.75);\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n\n.icon_play {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.icon_play ion-icon {\n  color: #fff;\n  font-size: 30px;\n}\n\n.blog_details {\n  margin-bottom: 50px;\n  margin-top: 20px;\n}\n\n.blog_details h1 {\n  font-size: 18px;\n}\n\nspan.writer {\n  font-size: 12px;\n  margin-top: 8px;\n  display: block;\n}\n\n.blog_details p {\n  font-size: 12px;\n  line-height: 19px;\n  margin-top: 32px;\n}\n\nspan.underline {\n  display: block;\n  text-decoration: underline;\n  margin: 10px 0;\n}\n\nvideo {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlkZW8tZGV0YWlscy92aWRlby1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHVDQUFBO0VBQ0Esc0NBQUE7RUFDQSxpQ0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUNEOztBQUNBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBRUo7O0FBQUE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQUdKOztBQURBO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUFJSjs7QUFGQTtFQUNDLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBS0Q7O0FBSEE7RUFDQyxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFNRDs7QUFKQTtFQUNDLFdBQUE7RUFDQSxlQUFBO0FBT0Q7O0FBTEE7RUFDQyxtQkFBQTtFQUNBLGdCQUFBO0FBUUQ7O0FBTkE7RUFDQyxlQUFBO0FBU0Q7O0FBUEE7RUFDQyxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFVRDs7QUFSQTtFQUNDLGVBQUE7RUFDRyxpQkFBQTtFQUNILGdCQUFBO0FBV0Q7O0FBVEE7RUFDQyxjQUFBO0VBQ0EsMEJBQUE7RUFDQSxjQUFBO0FBWUQ7O0FBVkE7RUFDSSxXQUFBO0FBYUoiLCJmaWxlIjoic3JjL2FwcC92aWRlby1kZXRhaWxzL3ZpZGVvLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsb2dfZGV0YWlsc19iZ3tcclxuXHRiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0ICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb3ZlciAhaW1wb3J0YW50O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uYnV0dG9uX2dyb3VwIHtcclxuICAgIG1heC13aWR0aDogNTBweDtcclxuICAgIG1hcmdpbjogMTBweCBhdXRvIDA7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuLmJhY2sge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbn1cclxuLmZvciB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbn1cclxuLmRhcmtfYmd7XHJcblx0YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjc1KTtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0dG9wOiAwO1xyXG5cdGxlZnQ6IDA7XHJcblx0cmlnaHQ6IDA7XHJcblx0Ym90dG9tOiAwO1xyXG59XHJcbi5pY29uX3BsYXl7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHRvcDogNTAlO1xyXG5cdGxlZnQ6IDUwJTtcclxuXHR0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1x0XHJcbn1cclxuLmljb25fcGxheSBpb24taWNvbntcclxuXHRjb2xvcjogI2ZmZjtcclxuXHRmb250LXNpemU6IDMwcHg7XHJcbn1cclxuLmJsb2dfZGV0YWlsc3tcclxuXHRtYXJnaW4tYm90dG9tOiA1MHB4O1xyXG5cdG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuLmJsb2dfZGV0YWlscyBoMXtcclxuXHRmb250LXNpemU6IDE4cHg7XHJcbn1cclxuc3Bhbi53cml0ZXJ7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdG1hcmdpbi10b3A6IDhweDtcclxuXHRkaXNwbGF5OiBibG9jaztcclxufVxyXG4uYmxvZ19kZXRhaWxzIHB7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcblx0bWFyZ2luLXRvcDogMzJweDtcclxufVxyXG5zcGFuLnVuZGVybGluZXtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHR0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuXHRtYXJnaW46IDEwcHggMDtcclxufVxyXG52aWRlbyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/video-details/video-details.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/video-details/video-details.page.ts ***!
  \*****************************************************/
/*! exports provided: VideoDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoDetailsPage", function() { return VideoDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _url_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url.service */ "./src/app/url.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");







let VideoDetailsPage = class VideoDetailsPage {
    constructor(router, netiveHttp, url, sanitizer, loadingController) {
        this.router = router;
        this.netiveHttp = netiveHttp;
        this.url = url;
        this.sanitizer = sanitizer;
        this.loadingController = loadingController;
    }
    go() {
        this.router.navigate(['tabs/Vedio']);
    }
    ngOnInit() {
        this.presentLoading();
        var VedioData = this.router.getCurrentNavigation().extras.state;
        var FinalData = VedioData.data;
        this.refressData = FinalData;
        this.title = FinalData.snippet.title;
        this.datetime = FinalData.snippet.publishTime;
        this.description = FinalData.snippet.description;
        this.GetVideoChile(FinalData.id.videoId);
    }
    presentLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
                duration: 2000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed!');
        });
    }
    GetVideoChile(videoid) {
        let url = this.url.Get_Video_ChildNode();
        let headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
        };
        this.netiveHttp.post(url, { 'video_id': videoid }, headers)
            .then(adata => {
            var data = JSON.parse(adata.data);
            var Result = data;
            this.viewCount = Result.video_details.statistics.viewCount;
            this.embedHtml = videoid;
        })
            .catch(error => {
            console.log(error);
        });
    }
    GetUrl(videoid) {
        return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + videoid);
    }
    GetDateTime(d) {
        var dt = new Date(d);
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
        return result;
    }
    ;
    doRefresh(event) {
        console.log('Begin async operation');
        this.GetVideoChile(this.refressData.id.videoId);
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }
};
VideoDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__["HTTP"] },
    { type: _url_service__WEBPACK_IMPORTED_MODULE_4__["UrlService"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] }
];
VideoDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-video-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./video-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/video-details/video-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./video-details.page.scss */ "./src/app/video-details/video-details.page.scss")).default]
    })
], VideoDetailsPage);



/***/ })

}]);
//# sourceMappingURL=video-details-video-details-module-es2015.js.map