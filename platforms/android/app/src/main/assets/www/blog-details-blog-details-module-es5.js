(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["blog-details-blog-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/blog-details/blog-details.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/blog-details/blog-details.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppBlogDetailsBlogDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\" class=\"header_icons\" (click)=\"go()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        SAFINA SOCIETY BLOG\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">     \r\n        <ion-icon name=\"chevron-back-outline\" style=\"color: #000 !important\"></ion-icon>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\t<div class=\"wrapper\">\r\n\t\t<div class=\"blog_details_bg\" style=\"background: url('{{img}}');\"></div>\r\n\t\t<div class=\"clearfix\"></div>\r\n\t\t<ion-grid>\r\n\t\t\t<ion-row>\r\n\t\t\t\t<ion-col size=\"12\">\r\n\t\t\t\t\t<div class=\"blog_details\">\r\n\t\t\t\t\t\t<h1>{{title}}</h1>\r\n\t\t\t\t\t\t<span class=\"writer\">By {{Auther}} · {{GetDateTime(pubdate)}}</span>\r\n\t\t\t\t\t\t<p>Bismillah al-Rahman al-Rahim \r\n\t\t\t\t\t\t<br><br>\r\n\t\t\t\t\t\t<div [innerHTML]=\"description\"></div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</ion-col>\r\n\t\t\t</ion-row>\r\n\t\t</ion-grid>\r\n\t</div>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/blog-details/blog-details-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/blog-details/blog-details-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: BlogDetailsPageRoutingModule */

    /***/
    function srcAppBlogDetailsBlogDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BlogDetailsPageRoutingModule", function () {
        return BlogDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _blog_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./blog-details.page */
      "./src/app/blog-details/blog-details.page.ts");

      var routes = [{
        path: '',
        component: _blog_details_page__WEBPACK_IMPORTED_MODULE_3__["BlogDetailsPage"]
      }];

      var BlogDetailsPageRoutingModule = function BlogDetailsPageRoutingModule() {
        _classCallCheck(this, BlogDetailsPageRoutingModule);
      };

      BlogDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], BlogDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/blog-details/blog-details.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/blog-details/blog-details.module.ts ***!
      \*****************************************************/

    /*! exports provided: BlogDetailsPageModule */

    /***/
    function srcAppBlogDetailsBlogDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BlogDetailsPageModule", function () {
        return BlogDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _blog_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./blog-details-routing.module */
      "./src/app/blog-details/blog-details-routing.module.ts");
      /* harmony import */


      var _blog_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./blog-details.page */
      "./src/app/blog-details/blog-details.page.ts");

      var BlogDetailsPageModule = function BlogDetailsPageModule() {
        _classCallCheck(this, BlogDetailsPageModule);
      };

      BlogDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _blog_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["BlogDetailsPageRoutingModule"]],
        declarations: [_blog_details_page__WEBPACK_IMPORTED_MODULE_6__["BlogDetailsPage"]]
      })], BlogDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/blog-details/blog-details.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/blog-details/blog-details.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppBlogDetailsBlogDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".blog_details_bg {\n  background-repeat: no-repeat !important;\n  background-position: center !important;\n  background-size: cover !important;\n  height: 220px;\n  width: 100%;\n}\n\n.blog_details {\n  margin-bottom: 50px;\n  margin-top: 20px;\n}\n\n.blog_details h1 {\n  font-size: 18px;\n}\n\nspan.writer {\n  font-size: 12px;\n  margin-top: 8px;\n  display: block;\n}\n\n.blog_details p {\n  line-height: 23px;\n  margin-top: 32px;\n}\n\nspan.underline {\n  display: block;\n  text-decoration: underline;\n  margin: 10px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmxvZy1kZXRhaWxzL2Jsb2ctZGV0YWlscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyx1Q0FBQTtFQUNBLHNDQUFBO0VBQ0EsaUNBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtBQUNEOztBQUNBO0VBQ0MsbUJBQUE7RUFDQSxnQkFBQTtBQUVEOztBQUFBO0VBQ0MsZUFBQTtBQUdEOztBQURBO0VBQ0MsZUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBSUQ7O0FBRkE7RUFDQyxpQkFBQTtFQUNBLGdCQUFBO0FBS0Q7O0FBSEE7RUFDQyxjQUFBO0VBQ0EsMEJBQUE7RUFDQSxjQUFBO0FBTUQiLCJmaWxlIjoic3JjL2FwcC9ibG9nLWRldGFpbHMvYmxvZy1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ibG9nX2RldGFpbHNfYmd7XHJcblx0YmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdCAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY292ZXIgIWltcG9ydGFudDtcclxuXHRoZWlnaHQ6IDIyMHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcbi5ibG9nX2RldGFpbHN7XHJcblx0bWFyZ2luLWJvdHRvbTogNTBweDtcclxuXHRtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcbi5ibG9nX2RldGFpbHMgaDF7XHJcblx0Zm9udC1zaXplOiAxOHB4O1xyXG59XHJcbnNwYW4ud3JpdGVye1xyXG5cdGZvbnQtc2l6ZTogMTJweDtcclxuXHRtYXJnaW4tdG9wOiA4cHg7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcbn1cclxuLmJsb2dfZGV0YWlscyBwe1xyXG5cdGxpbmUtaGVpZ2h0OiAyM3B4O1xyXG5cdG1hcmdpbi10b3A6IDMycHg7XHJcbn1cclxuc3Bhbi51bmRlcmxpbmV7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0dGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcblx0bWFyZ2luOiAxMHB4IDA7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/blog-details/blog-details.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/blog-details/blog-details.page.ts ***!
      \***************************************************/

    /*! exports provided: BlogDetailsPage */

    /***/
    function srcAppBlogDetailsBlogDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BlogDetailsPage", function () {
        return BlogDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var BlogDetailsPage = /*#__PURE__*/function () {
        function BlogDetailsPage(router, sanitizer) {
          _classCallCheck(this, BlogDetailsPage);

          this.router = router;
          this.sanitizer = sanitizer;
        }

        _createClass(BlogDetailsPage, [{
          key: "go",
          value: function go() {
            this.router.navigate(['tabs/Blog']);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var BlogData = this.router.getCurrentNavigation().extras.state;
            var FinalData = BlogData.data;
            this.title = FinalData.title || '';
            this.pubdate = FinalData.pubdate || '';
            this.description = FinalData.content || '';
            this.img = FinalData.image || '';
            this.Auther = FinalData.author || '';
          }
        }, {
          key: "GetDateTime",
          value: function GetDateTime(d) {
            var dt = new Date(d);
            var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
            return result;
          }
        }, {
          key: "getUrl",
          value: function getUrl(url) {
            return this.sanitizer.bypassSecurityTrustResourceUrl(url);
          }
        }]);

        return BlogDetailsPage;
      }();

      BlogDetailsPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]
        }];
      };

      BlogDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-blog-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./blog-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/blog-details/blog-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./blog-details.page.scss */
        "./src/app/blog-details/blog-details.page.scss"))["default"]]
      })], BlogDetailsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=blog-details-blog-details-module-es5.js.map