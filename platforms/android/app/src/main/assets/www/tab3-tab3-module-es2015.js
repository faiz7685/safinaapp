(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab3-tab3-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <!-- <img src=\"../../assets/images/logo.png\" alt=\"logo\" class=\"header_logo\"> -->\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        THE SAFINA SOCIETY PODCAST\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">\r\n        <!-- <ion-icon name=\"information-circle-outline\" size=\"medium\"></ion-icon> -->\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n  <div class=\"wrapper\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <div class=\"search_btn\">\r\n              <ion-input type=\"search\" placeholder=\"Search\" (keyup)=\"searchText(Search.value)\" #Search></ion-input>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <div class=\"categories\">\r\n            <ul>\r\n              <li id=\"Show0\" class=\"active jggg\"><a (click)=\"ShowAll(0)\">ALL</a></li>\r\n              <li id=\"Show3\" class=\"jggg\"><a (click)=\"ShowSeason3(3)\">Season 3</a></li>\r\n              <li id=\"Show2\" class=\"jggg\"><a (click)=\"ShowSeason2(2)\">Season 2</a></li>\r\n              <li id=\"Show1\" class=\"jggg\"><a (click)=\"ShowSeason1(1)\">Season 1</a></li>\r\n              <li id=\"Show4\" class=\"jggg\"><a (click)=\"ShowSOta(4)\">SOTA</a></li>\r\n            </ul>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid> \r\n\r\n    <ion-grid class=\"all m_10\" *ngIf=\"allBool\">\r\n      <ion-row>\r\n        <ion-col size=\"12\" *ngFor=\"let PodcastData of LetestPodcastDatas\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_body\" (click)=\"go(PodcastData)\">\r\n              <h2>{{PodcastData.title}}</h2>\r\n              <span class=\"date\">{{GetDateTime(PodcastData.pubdate)}}</span>\r\n              <p>{{PodcastData.description}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <div class=\"clearfix\"></div>\r\n\r\n\r\n    <ion-grid class=\"season3  m_10\" *ngIf=\"season3Bool\">\r\n      <ion-row>\r\n        <ion-col size=\"12\" *ngFor=\"let PodcastData of PodcastSeason3\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_body\" (click)=\"go(PodcastData)\">\r\n              <h2>{{PodcastData.title}}</h2>\r\n              <span class=\"date\">{{GetDateTime(PodcastData.pubdate)}}</span>\r\n              <p>{{PodcastData.description}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <div class=\"clearfix\"></div>\r\n\r\n\r\n    <ion-grid class=\"season2  m_10\" *ngIf=\"season2Bool\">\r\n      <ion-row>\r\n        <ion-col size=\"12\" *ngFor=\"let PodcastData of PodcastSeason2\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_body\" (click)=\"go(PodcastData)\">\r\n              <h2>{{PodcastData.title}}</h2>\r\n              <span class=\"date\">{{GetDateTime(PodcastData.pubdate)}}</span>\r\n              <p>{{PodcastData.description}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <div class=\"clearfix\"></div>\r\n\r\n\r\n    <ion-grid class=\"season1  m_10\" *ngIf=\"season1Bool\">\r\n      <ion-row>\r\n        <ion-col size=\"12\" *ngFor=\"let PodcastData of PodcastSeason1\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_body\" (click)=\"go(PodcastData)\">\r\n              <h2>{{PodcastData.title}}</h2>\r\n              <span class=\"date\">{{GetDateTime(PodcastData.pubdate)}}</span>\r\n              <p>{{PodcastData.description}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <div class=\"clearfix\"></div>\r\n\r\n\r\n    <ion-grid class=\"sota  m_10\" *ngIf=\"sotaBool\">\r\n      <ion-row>\r\n        <ion-col size=\"12\" *ngFor=\"let PodcastData of podcastSota\">\r\n          <div class=\"blog_card\">\r\n            <div class=\"blog_card_body\" (click)=\"go(PodcastData)\">\r\n              <h2>{{PodcastData.title}}</h2>\r\n              <span class=\"date\">{{GetDateTime(PodcastData.pubdate)}}</span>\r\n              <p>{{PodcastData.description}}</p>\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <div class=\"clearfix\"></div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/tab3/tab3-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tab3/tab3-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab3PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageRoutingModule", function() { return Tab3PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");




const routes = [
    {
        path: '',
        component: _tab3_page__WEBPACK_IMPORTED_MODULE_3__["Tab3Page"],
    }
];
let Tab3PageRoutingModule = class Tab3PageRoutingModule {
};
Tab3PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab3PageRoutingModule);



/***/ }),

/***/ "./src/app/tab3/tab3.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tab3-routing.module */ "./src/app/tab3/tab3-routing.module.ts");









let Tab3PageModule = class Tab3PageModule {
};
Tab3PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__["ExploreContainerComponentModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] }]),
            _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__["Tab3PageRoutingModule"],
        ],
        declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
    })
], Tab3PageModule);



/***/ }),

/***/ "./src/app/tab3/tab3.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".blog_card {\n  box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.13);\n  margin-bottom: 12px;\n}\n\n.blog_card_body {\n  margin-top: 0px;\n  padding: 6px 10px 10px;\n}\n\nspan.category {\n  background: #3A0A46;\n  display: block;\n  font-size: 10px;\n  max-width: 40%;\n  color: #fff;\n  text-transform: uppercase;\n  font-family: \"Gotham-Medium\";\n  text-align: center;\n  padding: 3px;\n}\n\n.blog_card_body h2 {\n  font-family: \"Gotham-Bold\";\n  font-size: 15px;\n}\n\n.blog_card_body p {\n  line-height: 16px;\n  font-size: 12px;\n  font-family: \"Lora-Regular\";\n  margin-top: 0px;\n}\n\nspan.date {\n  margin-top: 8px;\n  display: block;\n  font-size: 10px;\n  margin-bottom: 7px;\n  font-family: \"Gotham-Book\";\n}\n\n.search_btn {\n  margin-bottom: 20px;\n  margin-top: 10px;\n}\n\nion-input.sc-ion-input-md-h.sc-ion-input-md-s.md.hydrated {\n  box-shadow: 1px 2px 4px 1px rgba(213, 213, 213, 0.5);\n  border: 1px solid #d5d5d5;\n  font-family: \"Gotham-Medium\";\n}\n\n.categories ul {\n  padding: 0;\n  margin: 0 auto 5px;\n  display: table;\n  border-top: 1px solid #B1B1B1;\n}\n\n.categories ul li {\n  display: table-cell;\n  padding: 10px 11px 0;\n  font-size: 12px;\n  position: relative !important;\n}\n\n.categories ul li a {\n  text-decoration: none;\n  color: #B1B1B1;\n  font-family: \"Gotham-Bold\";\n}\n\n.categories ul li:first-child {\n  padding-left: 0px;\n}\n\n.categories ul li:last-child {\n  padding-right: 0px;\n}\n\n.active a {\n  color: #000 !important;\n}\n\n.active::after {\n  content: \"\";\n  width: 100%;\n  height: 1px;\n  background: #3A0A46;\n  position: absolute;\n  top: -1px;\n  left: 0;\n}\n\n.m_10 {\n  margin-bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiMy90YWIzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdDLCtDQUFBO0VBQ0EsbUJBQUE7QUFDRDs7QUFDQTtFQUNJLGVBQUE7RUFDQSxzQkFBQTtBQUVKOztBQUFBO0VBQ0MsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDRSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQUdIOztBQURBO0VBQ0csMEJBQUE7RUFDQSxlQUFBO0FBSUg7O0FBRkE7RUFDQyxpQkFBQTtFQUNBLGVBQUE7RUFDRywyQkFBQTtFQUNBLGVBQUE7QUFLSjs7QUFIQTtFQUNDLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0UsMEJBQUE7QUFNSDs7QUFKQTtFQUNDLG1CQUFBO0VBQ0EsZ0JBQUE7QUFPRDs7QUFMQTtFQUNJLG9EQUFBO0VBQ0EseUJBQUE7RUFDRCw0QkFBQTtBQVFIOztBQU5BO0VBQ0MsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLDZCQUFBO0FBU0Q7O0FBUEE7RUFDQyxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLDZCQUFBO0FBVUQ7O0FBUkE7RUFDQyxxQkFBQTtFQUNBLGNBQUE7RUFDRywwQkFBQTtBQVdKOztBQVRBO0VBQ0MsaUJBQUE7QUFZRDs7QUFWQTtFQUNDLGtCQUFBO0FBYUQ7O0FBWEE7RUFDQyxzQkFBQTtBQWNEOztBQVpBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0FBZUo7O0FBYkE7RUFDQyxtQkFBQTtBQWdCRCIsImZpbGUiOiJzcmMvYXBwL3RhYjMvdGFiMy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmxvZ19jYXJke1xyXG5cdC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDVweCA1cHggMHB4IHJnYmEoMCwwLDAsMC4xMyk7XHJcblx0LW1vei1ib3gtc2hhZG93OiAwcHggNXB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjEzKTtcclxuXHRib3gtc2hhZG93OiAwcHggNXB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjEzKTtcclxuXHRtYXJnaW4tYm90dG9tOiAxMnB4O1xyXG59XHJcbi5ibG9nX2NhcmRfYm9keXtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIHBhZGRpbmc6IDZweCAxMHB4IDEwcHg7XHJcbn1cclxuc3Bhbi5jYXRlZ29yeXtcclxuXHRiYWNrZ3JvdW5kOiAjM0EwQTQ2O1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG5cdGZvbnQtc2l6ZTogMTBweDtcclxuXHRtYXgtd2lkdGg6IDQwJTtcclxuXHRjb2xvcjogI2ZmZjtcclxuXHR0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICBmb250LWZhbWlseTogJ0dvdGhhbS1NZWRpdW0nO1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHBhZGRpbmc6IDNweDtcclxufVxyXG4uYmxvZ19jYXJkX2JvZHkgaDJ7XHJcbiAgIGZvbnQtZmFtaWx5OiAnR290aGFtLUJvbGQnO1xyXG4gICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuLmJsb2dfY2FyZF9ib2R5IHB7XHJcblx0bGluZS1oZWlnaHQ6IDE2cHg7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG4gICBcdGZvbnQtZmFtaWx5OiAnTG9yYS1SZWd1bGFyJztcclxuICAgXHRtYXJnaW4tdG9wOiAwcHg7XHJcbn1cclxuc3Bhbi5kYXRle1xyXG5cdG1hcmdpbi10b3A6IDhweDtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRmb250LXNpemU6IDEwcHg7XHJcblx0bWFyZ2luLWJvdHRvbTogN3B4O1xyXG4gICBmb250LWZhbWlseTogJ0dvdGhhbS1Cb29rJztcclxufVxyXG4uc2VhcmNoX2J0bntcclxuXHRtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cdG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuaW9uLWlucHV0LnNjLWlvbi1pbnB1dC1tZC1oLnNjLWlvbi1pbnB1dC1tZC1zLm1kLmh5ZHJhdGVkIHtcclxuICAgIGJveC1zaGFkb3c6IDFweCAycHggNHB4IDFweCByZ2JhKDIxMywgMjEzLCAyMTMsIDAuNSk7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZDVkNWQ1O1xyXG4gICBmb250LWZhbWlseTogJ0dvdGhhbS1NZWRpdW0nO1xyXG59XHJcbi5jYXRlZ29yaWVzIHVse1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0bWFyZ2luOiAwIGF1dG8gNXB4O1xyXG5cdGRpc3BsYXk6IHRhYmxlO1xyXG5cdGJvcmRlci10b3A6IDFweCBzb2xpZCAjQjFCMUIxO1xyXG59XHJcbi5jYXRlZ29yaWVzIHVsIGxpe1xyXG5cdGRpc3BsYXk6IHRhYmxlLWNlbGw7XHJcblx0cGFkZGluZzogMTBweCAxMXB4IDA7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xyXG59XHJcbi5jYXRlZ29yaWVzIHVsIGxpIGF7XHJcblx0dGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5cdGNvbG9yOiAjQjFCMUIxO1xyXG4gICBcdGZvbnQtZmFtaWx5OiAnR290aGFtLUJvbGQnO1xyXG59XHJcbi5jYXRlZ29yaWVzIHVsIGxpOmZpcnN0LWNoaWxke1xyXG5cdHBhZGRpbmctbGVmdDogMHB4O1xyXG59XHJcbi5jYXRlZ29yaWVzIHVsIGxpOmxhc3QtY2hpbGR7XHJcblx0cGFkZGluZy1yaWdodDogMHB4O1xyXG59XHJcbi5hY3RpdmUgYXtcclxuXHRjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xyXG59XHJcbi5hY3RpdmU6OmFmdGVye1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjM0EwQTQ2O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAtMXB4O1xyXG4gICAgbGVmdDogMDtcclxufVxyXG4ubV8xMHtcclxuXHRtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/tab3/tab3.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _url_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url.service */ "./src/app/url.service.ts");
/* harmony import */ var _loder_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../loder.service */ "./src/app/loder.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");







let Tab3Page = class Tab3Page {
    constructor(router, netiveHttp, url, ionLoader, el, loadingController) {
        this.router = router;
        this.netiveHttp = netiveHttp;
        this.url = url;
        this.ionLoader = ionLoader;
        this.el = el;
        this.loadingController = loadingController;
        this.PodcastSeason3 = [];
        this.PodcastSeason2 = [];
        this.PodcastSeason1 = [];
        this.PodcastAll = [];
        this.podcastSota = [];
        this.PodcastSeason3New = [];
        this.PodcastSeason2New = [];
        this.PodcastSeason1New = [];
        this.PodcastAllNew = [];
        this.podcastSotaNew = [];
        this.allBool = true;
        this.season3Bool = false;
        this.season2Bool = false;
        this.season1Bool = false;
        this.sotaBool = false;
        this.CurrentState = 0;
    }
    go(Podcast) {
        this.router.navigate(['audio-details'], { state: { data: Podcast } });
    }
    ngOnInit() {
        //this.GetPodcastPosts();
        this.Get_PodcastData();
    }
    ShowAll(id) {
        this.allBool = true;
        this.season3Bool = false;
        this.season2Bool = false;
        this.season1Bool = false;
        this.sotaBool = false;
        this.expendsubMenu(id);
        this.CurrentState = 0;
        this.searchText(this.searchData);
    }
    ShowSeason3(id) {
        this.allBool = false;
        this.season3Bool = true;
        this.season2Bool = false;
        this.season1Bool = false;
        this.sotaBool = false;
        this.expendsubMenu(id);
        this.CurrentState = 3;
        this.searchText(this.searchData);
    }
    ShowSeason2(id) {
        this.allBool = false;
        this.season3Bool = false;
        this.season2Bool = true;
        this.season1Bool = false;
        this.sotaBool = false;
        this.expendsubMenu(id);
        this.CurrentState = 2;
        this.searchText(this.searchData);
    }
    ShowSeason1(id) {
        this.allBool = false;
        this.season3Bool = false;
        this.season2Bool = false;
        this.season1Bool = true;
        this.sotaBool = false;
        this.expendsubMenu(id);
        this.CurrentState = 1;
        this.searchText(this.searchData);
    }
    ShowSOta(id) {
        this.allBool = false;
        this.season3Bool = false;
        this.season2Bool = false;
        this.season1Bool = false;
        this.sotaBool = true;
        this.expendsubMenu(id);
        this.CurrentState = 4;
        this.searchText(this.searchData);
    }
    expendsubMenu(id) {
        var elements = document.getElementsByClassName('jggg');
        elements[0].classList.remove('active');
        elements[1].classList.remove('active');
        elements[2].classList.remove('active');
        elements[3].classList.remove('active');
        elements[4].classList.remove('active');
        var parentbtnid = 'Show' + id;
        document.getElementById(parentbtnid).className += ' active';
    }
    GetPodcastPosts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message: 'Loading Data',
            });
            let url = this.url.Podcast_api();
            let headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"
            };
            this.netiveHttp.post(url, {}, headers)
                .then(adata => {
                var data = JSON.parse(adata.data);
                localStorage.setItem('PodcastData', adata.data);
                this.LetestPodcastDatas = data['item'];
                this.LetestPodcastDatasNew = data['item'];
                this.LetestPodcastDatas.forEach(element => {
                    var val = element.title.split(':')[0];
                    if (val == 'S2') {
                        this.PodcastSeason2.push(element);
                        this.PodcastSeason2New.push(element);
                    }
                    else if (val == 'S1') {
                        this.PodcastSeason1.push(element);
                        this.PodcastSeason1New.push(element);
                    }
                    else if (val == 'Stories Of The Awliya') {
                        this.podcastSota.push(element);
                        this.podcastSotaNew.push(element);
                    }
                    else if (val == 'S3') {
                        this.PodcastSeason3.push(element);
                        this.PodcastSeason3New.push(element);
                    }
                });
                this.loading.dismiss();
            })
                .catch(error => {
                console.log(error);
                this.loading.dismiss();
            });
            this.loading.present();
        });
    }
    Get_PodcastData() {
        var DataObj = localStorage.getItem('PodcastData');
        var FinalDataObj = JSON.parse(DataObj);
        this.LetestPodcastDatas = FinalDataObj['item'];
        this.LetestPodcastDatasNew = FinalDataObj['item'];
        this.LetestPodcastDatas.forEach(element => {
            var val = element.title.split(':')[0];
            if (val == 'S2') {
                this.PodcastSeason2.push(element);
                this.PodcastSeason2New.push(element);
            }
            else if (val == 'S1') {
                this.PodcastSeason1.push(element);
                this.PodcastSeason1New.push(element);
            }
            else if (val == 'Stories Of The Awliya') {
                this.podcastSota.push(element);
                this.podcastSotaNew.push(element);
            }
            else if (val == 'S3') {
                this.PodcastSeason3.push(element);
                this.PodcastSeason3New.push(element);
            }
        });
    }
    GetDateTime(d) {
        var dt = new Date(d);
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var result = mlist[dt.getMonth()] + ' ' + (dt.getDate() - 1) + ' ' + dt.getFullYear();
        return result;
    }
    ;
    doRefresh(event) {
        console.log('Begin async operation');
        this.GetPodcastPosts();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }
    searchText(e) {
        this.searchData = e;
        if (this.CurrentState === 0) {
            this.LetestPodcastDatas = this.ionLoader.filterItems(this.LetestPodcastDatasNew, e);
        }
        if (this.CurrentState === 1) {
            this.PodcastSeason1 = this.ionLoader.filterItems(this.PodcastSeason1New, e);
        }
        if (this.CurrentState === 2) {
            this.PodcastSeason2 = this.ionLoader.filterItems(this.PodcastSeason2New, e);
        }
        if (this.CurrentState === 3) {
            this.PodcastSeason3 = this.ionLoader.filterItems(this.PodcastSeason3New, e);
        }
        if (this.CurrentState === 4) {
            this.podcastSota = this.ionLoader.filterItems(this.podcastSotaNew, e);
        }
    }
};
Tab3Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_3__["HTTP"] },
    { type: _url_service__WEBPACK_IMPORTED_MODULE_4__["UrlService"] },
    { type: _loder_service__WEBPACK_IMPORTED_MODULE_5__["LoderService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] }
];
Tab3Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab3',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tab3.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tab3.page.scss */ "./src/app/tab3/tab3.page.scss")).default]
    })
], Tab3Page);



/***/ })

}]);
//# sourceMappingURL=tab3-tab3-module-es2015.js.map