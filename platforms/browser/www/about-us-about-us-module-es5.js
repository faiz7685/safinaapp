(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["about-us-about-us-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/about-us/about-us.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/about-us/about-us.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAboutUsAboutUsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\" class=\"header_icons\" (click)=\"go()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-buttons>\r\n      <ion-title class=\"header_title\">  \r\n        About Us\r\n      </ion-title>\r\n      <ion-buttons class=\"information\" slot=\"end\">     \r\n        <ion-icon name=\"chevron-back-outline\" style=\"color: #000 !important\"></ion-icon>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\t<div class=\"wrapper\">\r\n\t\t<div class=\"about_bg\">\r\n\t\t\t<div class=\"safina_logo\">\r\n\t\t\t\t<img src=\"./../assets/images/safina.png\" alt=\"safina_logo\">\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<ion-grid>\r\n\t\t\t<ion-row>\r\n\t\t\t\t<ion-col size=\"12\">\r\n\t\t\t\t\t<div class=\"about_details\">\r\n\t\t\t\t\t\t<p>Our presence on this earth is a test of wills. We are here to discover our Maker, to know and love Him through our own choice and volition. We also recognize that we are near the end of our umma’s history, and that keeping the faith is more often than not, an uphill challenge, for in the End Times, the umma is afflicted with the epidemic of loving dunya and hating death. \r\n\t\t\t\t\t\t<br><br>\r\n\r\n\t\t\t\t\t\tOur response is companionship and knowledge. We are dedicated to gaining knowledge and eliminating distractions. Our beliefs are based on the robust and firmly established teachings passed on by luminaries of the Sunna, Imam al-Nawawi and al-Ghazali, and those before them, Abu Hanifa, Malik, Shafi’, Ahmad, Bukhara, Muslim and al-Juwayni. \r\n\t\t\t\t\t\t<br><br>\r\n\r\n\t\t\t\t\t\tWe serve the entire family as well as every possible demographic. We do not seek to be huge. We care about quality over size and numbers. We are not slaves to numbers nor to our audience. We embrace technology and use it without allowing it to alter our vision, purpose or inner state. Through technology, we hope to bring like-minded students of knowledge together physically, not only virtually. We care as much about presentation as we do about content. Excellence, unicity of style along with simplicity is built into our DNA.</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</ion-col>\r\n\t\t\t\t<ion-col size=\"12\">\r\n\t\t\t\t\t<div class=\"social_media\">\r\n\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#!\">FACEBOOK</a></li>\r\n\t\t\t\t\t\t\t<li>|</li>\r\n\t\t\t\t\t\t\t<li><a href=\"#!\">INSTAGRAM</a></li>\r\n\t\t\t\t\t\t\t<li>|</li>\r\n\t\t\t\t\t\t\t<li><a href=\"#!\">TWITTER</a></li>\r\n\t\t\t\t\t\t\t<li>|</li>\r\n\t\t\t\t\t\t\t<li><a href=\"#!\">YOUTUBE</a></li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</ion-col>\r\n\t\t\t</ion-row>\r\n\t\t</ion-grid>\r\n\t\t<div class=\"powerd_by\">\r\n\t\t\t<ion-grid>\r\n\t\t\t\t<ion-row>\r\n\t\t\t\t\t<ion-col size=\"12\">\r\n\t\t\t\t\t\t<div class=\"powerd_text\">\r\n\t\t\t\t\t\t\t<p>This app is brought to you by <b>Talespun</b>, a software development & design firm. With a team deeply skilled in software development, data analytics, design, and agile delivery, Talespun is dedicated to transforming customer stories into incredible digital experiences.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</ion-col>\r\n\t\t\t\t</ion-row>\r\n\t\t\t</ion-grid>\r\n\t\t</div>\r\n\t</div>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/about-us/about-us-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/about-us/about-us-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: AboutUsPageRoutingModule */

    /***/
    function srcAppAboutUsAboutUsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AboutUsPageRoutingModule", function () {
        return AboutUsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _about_us_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./about-us.page */
      "./src/app/about-us/about-us.page.ts");

      var routes = [{
        path: '',
        component: _about_us_page__WEBPACK_IMPORTED_MODULE_3__["AboutUsPage"]
      }];

      var AboutUsPageRoutingModule = function AboutUsPageRoutingModule() {
        _classCallCheck(this, AboutUsPageRoutingModule);
      };

      AboutUsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AboutUsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/about-us/about-us.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/about-us/about-us.module.ts ***!
      \*********************************************/

    /*! exports provided: AboutUsPageModule */

    /***/
    function srcAppAboutUsAboutUsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AboutUsPageModule", function () {
        return AboutUsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _about_us_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./about-us-routing.module */
      "./src/app/about-us/about-us-routing.module.ts");
      /* harmony import */


      var _about_us_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./about-us.page */
      "./src/app/about-us/about-us.page.ts");

      var AboutUsPageModule = function AboutUsPageModule() {
        _classCallCheck(this, AboutUsPageModule);
      };

      AboutUsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _about_us_routing_module__WEBPACK_IMPORTED_MODULE_5__["AboutUsPageRoutingModule"]],
        declarations: [_about_us_page__WEBPACK_IMPORTED_MODULE_6__["AboutUsPage"]]
      })], AboutUsPageModule);
      /***/
    },

    /***/
    "./src/app/about-us/about-us.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/about-us/about-us.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppAboutUsAboutUsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".about_bg {\n  background: #000;\n  height: 135px;\n  width: 100%;\n  position: relative;\n}\n\n.safina_logo {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.safina_logo img {\n  max-width: 62px;\n  width: 100%;\n}\n\n.about_details {\n  margin-bottom: 22px;\n}\n\n.about_details p {\n  font-family: \"Lora-Regular\";\n  font-size: 12px;\n  line-height: 19px;\n}\n\n.social_media ul {\n  padding: 0px;\n  margin: 0px auto;\n}\n\n.social_media ul li {\n  display: inline-block;\n  font-family: \"Gotham-Bold\";\n  color: #000;\n  padding: 0 7px;\n  font-size: 10px;\n}\n\n.social_media ul li a {\n  color: #000;\n  text-decoration: none;\n}\n\n.powerd_by {\n  background: #F5F5F5;\n  padding: 20px;\n  margin-top: 30px;\n}\n\n.powerd_text p {\n  font-size: 10px;\n  line-height: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWJvdXQtdXMvYWJvdXQtdXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBQ0Q7O0FBQ0E7RUFDQyxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFFRDs7QUFBQTtFQUNDLGVBQUE7RUFDQSxXQUFBO0FBR0Q7O0FBREE7RUFDQyxtQkFBQTtBQUlEOztBQUZBO0VBQ0csMkJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFLSDs7QUFIQTtFQUNDLFlBQUE7RUFDQSxnQkFBQTtBQU1EOztBQUpBO0VBQ0kscUJBQUE7RUFDQSwwQkFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQU9KOztBQUxBO0VBQ0MsV0FBQTtFQUNBLHFCQUFBO0FBUUQ7O0FBTkE7RUFDQyxtQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQVNEOztBQVBBO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0FBVUQiLCJmaWxlIjoic3JjL2FwcC9hYm91dC11cy9hYm91dC11cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWJvdXRfYmd7XHJcblx0YmFja2dyb3VuZDogIzAwMDtcclxuXHRoZWlnaHQ6IDEzNXB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uc2FmaW5hX2xvZ297XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHRvcDogNTAlO1xyXG5cdGxlZnQ6IDUwJTtcclxuXHR0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1x0XHJcbn1cclxuLnNhZmluYV9sb2dvIGltZ3tcclxuXHRtYXgtd2lkdGg6IDYycHg7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn1cclxuLmFib3V0X2RldGFpbHN7XHJcblx0bWFyZ2luLWJvdHRvbTogMjJweDtcclxufVxyXG4uYWJvdXRfZGV0YWlscyBwe1xyXG4gICBmb250LWZhbWlseTogJ0xvcmEtUmVndWxhcic7XHJcbiAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbn1cclxuLnNvY2lhbF9tZWRpYSB1bHtcclxuXHRwYWRkaW5nOiAwcHg7XHJcblx0bWFyZ2luOiAwcHggYXV0bztcclxufVxyXG4uc29jaWFsX21lZGlhIHVsIGxpIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgXHRmb250LWZhbWlseTogJ0dvdGhhbS1Cb2xkJztcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgcGFkZGluZzogMCA3cHg7XHJcbiAgICBmb250LXNpemU6IDEwcHg7XHJcbn1cclxuLnNvY2lhbF9tZWRpYSB1bCBsaSBhe1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG4ucG93ZXJkX2J5e1xyXG5cdGJhY2tncm91bmQ6ICNGNUY1RjU7XHJcblx0cGFkZGluZzogMjBweDtcclxuXHRtYXJnaW4tdG9wOiAzMHB4O1xyXG59XHJcbi5wb3dlcmRfdGV4dCBwe1xyXG5cdGZvbnQtc2l6ZTogMTBweDtcclxuXHRsaW5lLWhlaWdodDogMTRweDtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/about-us/about-us.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/about-us/about-us.page.ts ***!
      \*******************************************/

    /*! exports provided: AboutUsPage */

    /***/
    function srcAppAboutUsAboutUsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AboutUsPage", function () {
        return AboutUsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var AboutUsPage = /*#__PURE__*/function () {
        function AboutUsPage(router) {
          _classCallCheck(this, AboutUsPage);

          this.router = router;
        }

        _createClass(AboutUsPage, [{
          key: "go",
          value: function go() {
            this.router.navigate(['tabs/Home']);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AboutUsPage;
      }();

      AboutUsPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      AboutUsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about-us',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./about-us.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/about-us/about-us.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./about-us.page.scss */
        "./src/app/about-us/about-us.page.scss"))["default"]]
      })], AboutUsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=about-us-about-us-module-es5.js.map