import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  private base_url="https://safinasocietyapp.com/api/v1/";

  constructor() { }

  public blog_feed_api(){
    //return 'https://www.safinasociety.org/blog-feed.xml';
    var url=this.base_url+'blogdetails';
    return url;
  }
 
  public Podcast_api(){
    var url=this.base_url+'podcastdetails';
    return url;
  }
  public video_api(){
    var url=this.base_url+'youtube';
    return url;
  }
  public event_api(){
    var url=this.base_url+'notificationsdetails';
    return url;
  }
  public Get_Video_ChildNode(){
    var url=this.base_url+'youtube-video-details';
    return url;
  }




}
