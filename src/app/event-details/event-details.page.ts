import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.page.html',
  styleUrls: ['./event-details.page.scss'],
})
export class EventDetailsPage implements OnInit {
  title:any;pubdate:any;description:any;img;any;Auther:any;
  constructor(private router: Router,private sanitizer:DomSanitizer) {}

  go() {
  	this.router.navigate(['tabs/Event'])
  }

  ngOnInit() {
    var EventData=this.router.getCurrentNavigation().extras.state;
    var FinalData=EventData.data;
    this.title=FinalData.title;
    this.pubdate=FinalData.pubdate;
    this.description=FinalData.content;
    this.img=FinalData.image;
  }
  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
    };
    getUrl(url){
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
