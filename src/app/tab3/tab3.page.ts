import { Component ,OnInit,ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { UrlService } from '../url.service';
import { LoderService } from '../loder.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{
  LetestPodcastDatas:any;PodcastSeason3:any=[];PodcastSeason2:any=[];PodcastSeason1:any=[];PodcastAll:any=[];
  podcastSota:any=[];
  LetestPodcastDatasNew:any;PodcastSeason3New:any=[];PodcastSeason2New:any=[];PodcastSeason1New:any=[];PodcastAllNew:any=[];
  podcastSotaNew:any=[];
  allBool:boolean=true;
  season3Bool:boolean=false;
  season2Bool:boolean=false;
  season1Bool:boolean=false;
  sotaBool:boolean=false;
  loading: any;
  CurrentState:any=0;
  searchData:any;

  constructor(private router: Router,
    private netiveHttp:HTTP,
    private url:UrlService,
    private ionLoader: LoderService,
    private el: ElementRef,
    private loadingController: LoadingController) {}

  go(Podcast) {
  	this.router.navigate(['audio-details'],{state: {data: Podcast}})
  }

  ngOnInit() {
    //this.GetPodcastPosts();
    this.Get_PodcastData();
  }

  ShowAll(id){
    this.allBool=true;
    this.season3Bool=false;
    this.season2Bool=false;
    this.season1Bool=false;
    this.sotaBool=false;
    this.expendsubMenu(id);
    this.CurrentState=0;
    this.searchText(this.searchData);
  }
  ShowSeason3(id){
    this.allBool=false;
    this.season3Bool=true;
    this.season2Bool=false;
    this.season1Bool=false;
    this.sotaBool=false;
    this.expendsubMenu(id);
    this.CurrentState=3;
    this.searchText(this.searchData);
  }
  ShowSeason2(id){
    this.allBool=false;
    this.season3Bool=false;
    this.season2Bool=true;
    this.season1Bool=false;
    this.sotaBool=false;
    this.expendsubMenu(id);
    this.CurrentState=2;
    this.searchText(this.searchData);
  }
  ShowSeason1(id){
    this.allBool=false;
    this.season3Bool=false;
    this.season2Bool=false;
    this.season1Bool=true;
    this.sotaBool=false;
    this.expendsubMenu(id);
    this.CurrentState=1;
    this.searchText(this.searchData);
  }
  ShowSOta(id){
    this.allBool=false;
    this.season3Bool=false;
    this.season2Bool=false;
    this.season1Bool=false;
    this.sotaBool=true
    this.expendsubMenu(id);
    this.CurrentState=4;
    this.searchText(this.searchData);
  }

  expendsubMenu(id: number) {
    var elements = document.getElementsByClassName('jggg');
    elements[0].classList.remove('active');
    elements[1].classList.remove('active');
    elements[2].classList.remove('active');
    elements[3].classList.remove('active');
    elements[4].classList.remove('active');
    var parentbtnid = 'Show' + id;
    document.getElementById(parentbtnid).className += ' active'
  }


  async GetPodcastPosts() {
    this.loading = await this.loadingController.create({
      message: 'Loading Data',
    });
    let url=this.url.Podcast_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('PodcastData',adata.data);
      this.LetestPodcastDatas=data['item'];
      this.LetestPodcastDatasNew=data['item'];
      this.LetestPodcastDatas.forEach(element =>{
        var val= element.title.split(':')[0];
        if(val == 'S2'){
          this.PodcastSeason2.push(element);
          this.PodcastSeason2New.push(element);
        }
        else if(val == 'S1'){
          this.PodcastSeason1.push(element);
          this.PodcastSeason1New.push(element);
        }
        else if(val == 'Stories Of The Awliya'){
          this.podcastSota.push(element);
          this.podcastSotaNew.push(element);
        }
        else if(val == 'S3'){
          this.PodcastSeason3.push(element);
          this.PodcastSeason3New.push(element);
        }
      });
      this.loading.dismiss();
    })
    .catch(error => {
      console.log(error);
      this.loading.dismiss();
    });
    this.loading.present();
  }

  Get_PodcastData(){
    var DataObj=localStorage.getItem('PodcastData');
    var FinalDataObj=JSON.parse(DataObj);
    this.LetestPodcastDatas=FinalDataObj['item'];
      this.LetestPodcastDatasNew=FinalDataObj['item'];
      this.LetestPodcastDatas.forEach(element =>{
        var val= element.title.split(':')[0];
        if(val == 'S2'){
          this.PodcastSeason2.push(element);
          this.PodcastSeason2New.push(element);
        }
        else if(val == 'S1'){
          this.PodcastSeason1.push(element);
          this.PodcastSeason1New.push(element);
        }
        else if(val == 'Stories Of The Awliya'){
          this.podcastSota.push(element);
          this.podcastSotaNew.push(element);
        }
        else if(val == 'S3'){
          this.PodcastSeason3.push(element);
          this.PodcastSeason3New.push(element);
        }
      });
  }

  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
  };
  doRefresh(event) {
    console.log('Begin async operation');
    this.GetPodcastPosts();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

    searchText(e){
      this.searchData=e;
      if(this.CurrentState===0){
        this.LetestPodcastDatas=this.ionLoader.filterItems(this.LetestPodcastDatasNew,e);
      }
      if(this.CurrentState===1){
        this.PodcastSeason1=this.ionLoader.filterItems(this.PodcastSeason1New,e);
      }
      if(this.CurrentState===2){
        this.PodcastSeason2=this.ionLoader.filterItems(this.PodcastSeason2New,e);
      }
      if(this.CurrentState===3){
        this.PodcastSeason3=this.ionLoader.filterItems(this.PodcastSeason3New,e);
      }
      if(this.CurrentState===4){
        this.podcastSota=this.ionLoader.filterItems(this.podcastSotaNew,e);
      }
     
    }
}
