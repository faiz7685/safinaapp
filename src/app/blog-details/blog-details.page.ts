import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.page.html',
  styleUrls: ['./blog-details.page.scss'],
})
export class BlogDetailsPage implements OnInit {
  title:any;pubdate:any;description:any;img;any;Auther:any;
  constructor(private router: Router,private sanitizer:DomSanitizer) {}

  go() {
  	this.router.navigate(['tabs/Blog'])
  }

  ngOnInit() {
    var BlogData=this.router.getCurrentNavigation().extras.state;
    var FinalData=BlogData.data;
    this.title=FinalData.title||'';
    this.pubdate=FinalData.pubdate||'';
    this.description=FinalData.content||'';
    this.img=FinalData.image||'';
    this.Auther=FinalData.author||'';
  }
  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
    };
    getUrl(url){
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

}
