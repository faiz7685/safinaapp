import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { UrlService } from '../url.service';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-video-details',
  templateUrl: './video-details.page.html',
  styleUrls: ['./video-details.page.scss'],
})
export class VideoDetailsPage implements OnInit {
  title:any;view:any;datetime:any;description:any;videoid:any;
  viewCount:any;embedHtml:any;refressData:any;

  constructor(private router: Router,
    private netiveHttp:HTTP,
    private url:UrlService,
    private sanitizer:DomSanitizer,
    public loadingController: LoadingController) {
  }

  go() {
    this.router.navigate(['tabs/Vedio'])
  }
  ngOnInit() {
    this.presentLoading();
    var VedioData=this.router.getCurrentNavigation().extras.state;
    var FinalData=VedioData.data;
    this.refressData=FinalData;
    this.title=FinalData.snippet.title;
    this.datetime=FinalData.snippet.publishTime;
    this.description=FinalData.snippet.description;
    this.GetVideoChile(FinalData.id.videoId);
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
  GetVideoChile(videoid){
    let url=this.url.Get_Video_ChildNode();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{'video_id':videoid},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      var Result=data;
        this.viewCount=Result.video_details.statistics.viewCount;
        this.embedHtml=videoid;
    })
    .catch(error => {
      console.log(error);
    });
  }
 
  GetUrl(videoid){
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+videoid);
  }

  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
    };
    doRefresh(event) {
      console.log('Begin async operation');
      this.GetVideoChile(this.refressData.id.videoId);
      setTimeout(() => {
        console.log('Async operation has ended');
        event.target.complete();
      }, 2000);
    }
}
