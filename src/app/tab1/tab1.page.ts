import { Component ,OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { UrlService } from '../url.service';
import { ToastrService } from 'ngx-toastr';
import { LoadingController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  LetestBlogTitle:any;LetestBlogTime:any;LetestBlogimg:any;LetestBlogDesc:any;GotoBlogData:any;
  LetestVideoTitle:any;LetestVideoTime:any;LetestVideoImg:any;LetestVideoDesc:any;GotoVedioData:any;
  LetestPodcastTitle:any;LetestPodcastTime:any;LetestPodcastImg:any;LetestPodcastDesc:any;LetestPodcastDatas:any;
  LetestEventTitle:any;LetestEventTime:any;LetestEventImg:any;LetestEventDesc:any;LetestEventsDatas:any;
  loading: any;
  constructor(private router: Router,
    private netiveHttp:HTTP,
    private url:UrlService,
    private toastr: ToastrService,
    public loadingController: LoadingController,
    private sanitizer:DomSanitizer,
    ) {}

  ngOnInit() {
    this.GetBlogPosts();
    this.GetPodcastPosts();
    this.GetVideoPosts();
    this.GetEventsPosts();
  }
  async GetBlogPosts(){
    this.loading = await this.loadingController.create({
      message: 'Loading Data',
    });
    let url=this.url.blog_feed_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('BlogData',adata.data);
      this.GotoBlogData=data['item'][0];
      console.log(data['item'][0].image);
      this.LetestBlogTitle=data['item'][0].title;
      this.LetestBlogTime=data['item'][0].pubdate;
      this.LetestBlogimg=data['item'][0].image;
      this.LetestBlogDesc=data['item'][0].description; 
      this.loading.dismiss(); 
    })
    .catch(error => {
      console.log(error);this.loading.dismiss();
    });
    this.loading.present();
  }
  GotoBlog(){
    this.router.navigate(['blog-details'],{state: {data: this.GotoBlogData}});
  }
  GetVideoPosts(){
    let url=this.url.video_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('VideoData',adata.data);
      this.GotoVedioData=data['details'].results[0];
      this.LetestVideoTitle=data['details'].results[0].snippet.title;
      this.LetestVideoTime=data['details'].results[0].snippet.publishTime;
      this.LetestVideoImg=data['details'].results[0].snippet.thumbnails.default.url;
      this.LetestVideoDesc=data['details'].results[0].snippet.description; 
    })
    .catch(error => {
      console.log(error);
    });
    
  }
  GotoVedio(){
    this.router.navigate(['video-details'],{state: {data: this.GotoVedioData}});
  }

  GetEventsPosts(){
    let url=this.url.event_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('EventData',adata.data);
      this.LetestEventsDatas=data['item'][0];
      this.LetestEventTitle=data['item'][0].title;
      this.LetestEventTime=data['item'][0].pubdate;
      this.LetestEventImg=data['item'][0].image;
      this.LetestEventDesc=data['item'][0].description;
    })
    .catch(error => {
      console.log(error);
    });
    
  }
  GotoEvents(){
    this.router.navigate(['event-details'],{state: {data: this.LetestEventsDatas}});
  }

  GetPodcastPosts(){
    let url=this.url.Podcast_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('PodcastData',adata.data);
      this.LetestPodcastDatas=data['item'][0];
      this.LetestPodcastTitle=data['item'][0].title;
      this.LetestPodcastTime=data['item'][0].pubdate;
      this.LetestPodcastImg=data['item'][0].banner;
      this.LetestPodcastDesc=data['item'][0].description;
    })
    .catch(error => {
      console.log(error);
    });
    
  }
  GotoPodcast(){
    this.router.navigate(['audio-details'],{state: {data: this.LetestPodcastDatas}});
  }
  go() {
  	this.router.navigate(['about-us'])
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.GetBlogPosts();
    this.GetPodcastPosts();
    this.GetVideoPosts();
    this.GetEventsPosts();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
    };
  getUrl(url){
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
