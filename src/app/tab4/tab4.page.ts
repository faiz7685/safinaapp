import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { UrlService } from '../url.service';
import { LoadingController } from '@ionic/angular';
import { LoderService } from '../loder.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  LetestVideoDatas:any;loading: any;
  LetestVideoDatasNew:any;
  token:any='';
  constructor(
    private router: Router,
    private netiveHttp:HTTP,
    private url:UrlService,
    public loadingController: LoadingController,
    private ionLoader: LoderService
    ) {}

  go(VideoData) {
    this.router.navigate(['video-details'],{state: {data: VideoData}});
  }
  ngOnInit() {
    //this.GetVideoPosts();
    this.Get_VideoData();
  }

  async GetVideoPosts(){
    this.loading = await this.loadingController.create({
      message: 'Loading Data',
    });
    let url=this.url.video_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('VideoData',adata.data);
      this.LetestVideoDatas=data['details'].results;
      this.LetestVideoDatasNew=data['details'].results;
      this.token=data['details'].info.nextPageToken
      this.loading.dismiss();
    })
    .catch(error => {
      console.log(error);
      this.loading.dismiss();
    });
      this.loading.present();
  }

  Get_VideoData(){
    var DataObj=localStorage.getItem('VideoData');
    var FinalDataObj=JSON.parse(DataObj);
    this.LetestVideoDatas=FinalDataObj['details'].results;
    this.LetestVideoDatasNew=FinalDataObj['details'].results;
    this.token=FinalDataObj['details'].info.nextPageToken
  }

  async GetMoreVideos(){
    this.loading = await this.loadingController.create({
      message: 'Loading Data',
    });
    let url=this.url.video_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{'next_page_token':this.token},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      var finalData=data;
      var item=finalData['details'].results;
      this.token=finalData['details'].info.nextPageToken
      item.forEach(element => {
        this.LetestVideoDatas.push(element);
        this.LetestVideoDatasNew.push(element);
      });
      console.log(this.LetestVideoDatas);
      this.loading.dismiss();
    })
    .catch(error => {
      console.log(error);
      this.loading.dismiss();
    });
    this.loading.present();
  }
  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
    };
  doRefresh(event) {
    console.log('Begin async operation');
    this.GetVideoPosts();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  searchText(e){
   this.LetestVideoDatas=this.ionLoader.filterItemsforYoutube(this.LetestVideoDatasNew,e);
  }

}
