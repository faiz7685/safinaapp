import { Component ,OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { UrlService } from '../url.service';
import { LoadingController } from '@ionic/angular';
import { LoderService } from '../loder.service';
import { FormsModule }   from '@angular/forms';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{
  LetestBlogDatas:any;loading: any;
  LetestBlogDatasNew:any;
  constructor(
    private router: Router,
    private netiveHttp:HTTP,
    private url:UrlService,
    public loadingController: LoadingController,
    private ionLoader: LoderService) {}

  go(BlogData) {
  	this.router.navigate(['blog-details'],{state: {data: BlogData}})
  }

  ngOnInit() {
    //this.GetBlogPosts();
    this.Get_BlogData();
  }

  async GetBlogPosts(){
    this.loading = await this.loadingController.create({
      message: 'Loading Data',
    });
    let url=this.url.blog_feed_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.netiveHttp.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('BlogData',adata.data);
      this.LetestBlogDatas=data['item'];
      this.LetestBlogDatasNew=data['item'];
      this.loading.dismiss();
    })
    .catch(error => {
      console.log(error);
      this.loading.dismiss();
    });
      this.loading.present();
  }
  Get_BlogData(){
    var DataObj=localStorage.getItem('BlogData');
    var FinalDataObj=JSON.parse(DataObj);
    this.LetestBlogDatas=FinalDataObj['item'];
      this.LetestBlogDatasNew=FinalDataObj['item'];
  }
  doRefresh(event) {
    console.log('Begin async operation');
    this.GetBlogPosts();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
  };
  searchText(e){
   this.LetestBlogDatas=this.ionLoader.filterItems(this.LetestBlogDatasNew,e);
  }

}
