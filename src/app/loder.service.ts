import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoderService {
isLoading = false;
  constructor(public loadingController: LoadingController) { }


  filterItems(obj,searchTerm) {
    return obj.filter(item => {
      return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  filterItemsforYoutube(obj,searchTerm) {
    return obj.filter(item => {
      return item.snippet.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

}
