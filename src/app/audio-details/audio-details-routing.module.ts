import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AudioDetailsPage } from './audio-details.page';

const routes: Routes = [
  {
    path: '',
    component: AudioDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AudioDetailsPageRoutingModule {}
