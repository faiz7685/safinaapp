import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AudioDetailsPageRoutingModule } from './audio-details-routing.module';

import { AudioDetailsPage } from './audio-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AudioDetailsPageRoutingModule
  ],
  declarations: [AudioDetailsPage]
})
export class AudioDetailsPageModule {}
