import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-audio-details',
  templateUrl: './audio-details.page.html',
  styleUrls: ['./audio-details.page.scss'],
})
export class AudioDetailsPage implements OnInit {
  title:any;DateTime:any;audiolink:any;summary:any;author:any;
  finalUrl:any;
  //sanitizer: any;
  constructor(private router: Router,private sanitizer:DomSanitizer) {}

  go() {
  	this.router.navigate(['tabs/Podcast'])
  }

  ngOnInit() {
    var PodcastData=this.router.getCurrentNavigation().extras.state;
    var FinalData=PodcastData.data;
    //console.log(FinalData);
    this.title=FinalData.title;
    this.DateTime=FinalData.pubdate;
    this.audiolink=this.sanitizer.bypassSecurityTrustResourceUrl(FinalData.audio_link);
    this.summary=FinalData.summary;
    this.author=FinalData.author;
  }
  // getSafeUrl(url) {
	// 	//this.finalUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);		
  // }
  
  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
    };
}
