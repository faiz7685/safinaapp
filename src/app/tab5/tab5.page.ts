import { Component ,OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
//import { HttpClient } from '@angular/common/http';
import { UrlService } from '../url.service';
import { LoadingController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {
	LetestEventsDatas:any;loading: any;
  constructor(private router: Router,
    private http:HTTP,
    private url:UrlService,
    public loadingController: LoadingController,private sanitizer:DomSanitizer) {}

  go(Event) {
  	this.router.navigate(['event-details'],{state: {data: Event}})
  }
  ngOnInit() {
    this.Get_EventData();
    //this.GetEventsPosts();
  }

  async GetEventsPosts(){
    this.loading = await this.loadingController.create({
      message: 'Loading Data',
    });
    let url=this.url.event_api();
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8" , 
      "Access-Control-Allow-Origin": "*", 
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With",
      "Access-Control-Allow-Credentials" : "true",
      "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT, OPTIONS, TRACE, PATCH, CONNECT"  
    };
    this.http.post(url,{},headers)
    .then(adata => {
      var data=JSON.parse(adata.data);
      localStorage.setItem('EventData',adata.data);
      this.LetestEventsDatas=data['item'];
        this.loading.dismiss();
    })
    .catch(error => {
      console.log(error);
      this.loading.dismiss();
    });
      this.loading.present();
  }

  Get_EventData(){
    var DataObj=localStorage.getItem('EventData');
    var FinalDataObj=JSON.parse(DataObj);
    this.LetestEventsDatas=FinalDataObj['item'];
  }

  GetDateTime (d){
    var dt=new Date(d);
    var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var result=mlist[dt.getMonth()]+' '+ (dt.getDate()-1)+' '+dt.getFullYear();
      return result;
    };
    doRefresh(event) {
      console.log('Begin async operation');
      this.GetEventsPosts();
      setTimeout(() => {
        console.log('Async operation has ended');
        event.target.complete();
      }, 2000);
    }
    getUrl(url){
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
